﻿using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;

public enum OBannerAdPlacement
{	None,
	Top,
	Bottom,	
};

public class OInMobiHandler : Singleton<OInMobiHandler>
{
	#if UNITY_IPHONE
	[DllImport ("__Internal")]
	private static extern void _InitInMobi(string adUnitID);
	[DllImport ("__Internal")]
	private static extern void _CreateBannerAd(string adUnitID);
	[DllImport ("__Internal")]
	private static extern void _CreateBottomBannerAd(string adUnitID);
	[DllImport ("__Internal")]
	private static extern void _RemoveBannerAd();
	[DllImport ("__Internal")]
	private static extern void _CreateInterstitialAd(string adUnitID);
	[DllImport ("__Internal")]
	private static extern bool _IsInterstitialAdAvailable();
	[DllImport ("__Internal")]
	private static extern void _ShowInterstitialAd();	
	#endif

	#if UNITY_ANDROID
	private static AndroidJavaObject jo;
	#endif
	
	public delegate void Action();
	public static Action OnBannerDidReceiveAd = delegate {};
	public static Action OnBannerDidFailToReceiveAd = delegate {};
	public static Action OnBannerDidInteract = delegate {};
	public static Action OnBannerWillPresentScreen = delegate {};
	public static Action OnBannerDidDismissScreen = delegate {};
	public static Action OnBannerWillLeaveApplication = delegate {};
	public static Action OnInterstitialDidReceiveAd = delegate {};
	public static Action OnInterstitialDidFailToReceiveAd = delegate {};
	public static Action OnInterstitialDidInteract = delegate {};
	public static Action OnInterstitialWillPresentScreen = delegate {};
	public static Action OnInterstitialWillDismissScreen = delegate {};
	public static Action OnInterstitialWillLeaveApplication = delegate {};
	
	public void InitMgr()
	{	
		if (!Application.isEditor)
		{	
			#if UNITY_IPHONE
			_InitInMobi(OGameMgr.Instance.INMOBI_IOS);
			#endif
			
			#if UNITY_ANDROID
			jo = new AndroidJavaObject("com.orangenose.inmobi.InMobiPlugin");
			jo.Call("InitInMobi", OGameMgr.Instance.INMOBI_ANDROID);
			#endif
		}	else
		{	Debug.Log("OInMobiHandler :: InMobi does not run in Editor.");
		}	
	}

	public void createBannerAd()
	{	
		if (!Application.isEditor)
		{	
			#if UNITY_IPHONE
			_CreateBannerAd(OGameMgr.Instance.INMOBI_IOS);
			#endif
			
			#if UNITY_ANDROID
			jo.Call("CreateBannerAd", OGameMgr.Instance.INMOBI_ANDROID);
			#endif
		}	else
		{	Debug.Log("OInMobiHandler :: InMobi does not run in Editor.");
		}
	}
	
	public void createBottomBannerAd()
	{	
		if (!Application.isEditor)
		{	
			#if UNITY_IPHONE
			_CreateBottomBannerAd(OGameMgr.Instance.INMOBI_IOS);
			#endif
			
			#if UNITY_ANDROID
			jo.Call("CreateBottomBannerAd", OGameMgr.Instance.INMOBI_ANDROID);
			#endif
		}	else
		{	Debug.Log("OInMobiHandler :: InMobi does not run in Editor.");
		}
	}
	
	public void removeBannerAd()
	{	
		if (!Application.isEditor)
		{	
			#if UNITY_IPHONE
			_RemoveBannerAd();
			#endif

			#if UNITY_ANDROID
			jo.Call("RemoveBannerAd");
			#endif
			
		}	else
		{	Debug.Log("OInMobiHandler :: InMobi does not run in Editor.");
		}
	}

	public void createInterstitialAd()
	{	
		if (!Application.isEditor)
		{	
			#if UNITY_IPHONE
			_CreateInterstitialAd(OGameMgr.Instance.INMOBI_IOS);
			#endif

			#if UNITY_ANDROID
			jo.Call("CreateInterstitialAd", OGameMgr.Instance.INMOBI_ANDROID);
			#endif
			
		}	else
		{	Debug.Log("OInMobiHandler :: InMobi does not run in Editor.");
		}
	}
	
	public bool isInterstitialAdAvailable()
	{
		if (!Application.isEditor)
		{	
			#if UNITY_IPHONE
			return _IsInterstitialAdAvailable();
			#endif
			
			#if UNITY_ANDROID
			return jo.Call<bool>("IsInterstitialAdAvailable");
			#else
			return false;
			#endif
		}	else
		{	Debug.Log("OInMobiHandler :: InMobi does not run in Editor.");
			return false;
		}
	}

	public void showInterstitialAd()
	{	
		if (!Application.isEditor)
		{	
			#if UNITY_IPHONE
			_ShowInterstitialAd();
			#endif

			#if UNITY_ANDROID
			jo.Call("ShowInterstitialAd");
			#endif
			
		}	else
		{	Debug.Log("OInMobiHandler :: InMobi does not run in Editor.");
		}
	}

	// CALLBACKS
	void bannerDidReceiveAd()
	{	
		Debug.Log("OInMobiHandler :: bannerDidReceiveAd");
		OnBannerDidReceiveAd();
	}

	void bannerDidFailToReceiveAd()
	{
		Debug.Log("OInMobiHandler :: bannerDidFailToReceiveAd");
		OnBannerDidFailToReceiveAd();
	}

	void bannerDidInteract()
	{
		Debug.Log("OInMobiHandler :: bannerDidInteract");
		OnBannerDidInteract();
	}

	void bannerWillPresentScreen()
	{
		Debug.Log("OInMobiHandler :: bannerWillPresentScreen");
		OnBannerWillPresentScreen();
	}

	void bannerDidDismissScreen()
	{
		Debug.Log("OInMobiHandler :: bannerDidDismissScreen");
		OnBannerDidDismissScreen();
	}

	void bannerWillLeaveApplication()
	{
		Debug.Log("OInMobiHandler :: bannerWillLeaveApplication");
		OnBannerWillLeaveApplication();
	}

	void interstitialDidReceiveAd()
	{	
		Debug.Log("OInMobiHandler :: interstitialDidReceiveAd");
		OnInterstitialDidReceiveAd();
	}
	
	void interstitialDidFailToReceiveAd()
	{
		Debug.Log("OInMobiHandler :: didFailToReceiveAd");
		OnInterstitialDidFailToReceiveAd();
	} 

	void interstitialDidInteract()
	{
		Debug.Log("OInMobiHandler :: interstitialDidInteract");
		OnInterstitialDidInteract();
	} 
	
	void interstitialWillPresentScreen()
	{
		Debug.Log("OInMobiHandler :: interstitialWillPresentScreen");
		OnInterstitialWillPresentScreen();
	} 	

	void interstitialWillDismissScreen()
	{
		Debug.Log("OInMobiHandler :: interstitialWillDismissScreen");
		OnInterstitialWillDismissScreen();
	} 

	void interstitialWillLeaveApplication()
	{
		Debug.Log("OInMobiHandler :: interstitialWillLeaveApplication");
		OnInterstitialWillLeaveApplication();
	} 

	
}
