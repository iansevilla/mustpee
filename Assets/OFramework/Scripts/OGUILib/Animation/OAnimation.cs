﻿using UnityEngine;
using System.Collections;

public abstract class OAnimation : MonoBehaviour {

	public string animationId = "default";
	public bool isPlayOnEnable = false;
	public bool isResetOnPlay = true;
	public bool isResetOnDisable = true;
	
	// This is for convenience -- inheriting classes have the option to ignore this.
	public float animationDuration = 1f;

	// These base animation settings will be applied all at once when calling ApplyBaseSettings.
	// This is for convenience -- inheriting classes have the option to ignore this.
#region Base Animation Settings
	public float animationBaseDelay = 0f;
	public LeanTweenType animationBaseEase = LeanTweenType.linear;
	public LeanTweenType animationBaseLoopType = LeanTweenType.once;
#endregion
	
	public static void PlayAnimationsInChildren(GameObject root, string id)
	{
		OAnimation[] anims = root.GetComponentsInChildren<OAnimation>();
		
		foreach (OAnimation anim in anims)
		{
			if (anim.animationId == id) {
				anim.PlayAnimation();
			}
		}
	}

	public virtual void OnEnable()
	{
		if (isPlayOnEnable) {
			PlayAnimation();
		}
	}
	
	public void PlayAnimation()
	{
		if (isResetOnPlay)
			ResetAnimation();
			
		DoAnimation();
	}
	
	public virtual void OnDisable()
	{
		LeanTween.cancel(gameObject);
		
		if (isResetOnDisable)
			ResetAnimation();
	}
	
	public LTDescr ApplyBaseSettings(LTDescr tween)
	{
		return tween.setDelay(animationBaseDelay).setEase(animationBaseEase).setLoopType(animationBaseLoopType);
	}
	
	public abstract void ResetAnimation();
	
	public abstract void DoAnimation();
}
