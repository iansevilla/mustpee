﻿/*
 * + DAM - Spread.cs
 * 
 * Manual Spread of object position in 2D, put this on GameObject desired
 * 
 * Params
 * - x_percentage - position x percentage from bottom left
 * - y_percentage - position y percentage from bottom left 
 */

using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class OSpread : MonoBehaviour {

	//+ DAM - reposition based from x and y percentage
	void Awake () 
	{	setSpread (this.transform);
	}

	// Spread calculation based from Aspect Ratio Graph
	public static void setSpread(Transform objectTransform)
	{
		if ((Screen.orientation == ScreenOrientation.Portrait || Screen.orientation == ScreenOrientation.PortraitUpsideDown)) 
		{	
			float aspectRatio = OResolutionHandler.screenHeight()/OResolutionHandler.screenWidth();
			
			if (aspectRatio >= 1.7f){	
			}
			else if (aspectRatio >= 1.5f){	
				objectTransform.localPosition = new Vector3( objectTransform.localPosition.x * 1.18f,
				                                             objectTransform.localPosition.y,
				                                             objectTransform.localPosition.z );
			}
			else
			{	objectTransform.localPosition = new Vector3( objectTransform.localPosition.x * 1.33f,
				                                             objectTransform.localPosition.y,
				                                             objectTransform.localPosition.z );
			}
		}
		else
		{	float aspectRatio = OResolutionHandler.screenWidth()/OResolutionHandler.screenHeight();
			
			if (aspectRatio >= 1.7f){	
			}
			else if (aspectRatio >= 1.5f) { 	
				objectTransform.localPosition = new Vector3( objectTransform.localPosition.x * 0.84f,
				                                             objectTransform.localPosition.y,
				                                             objectTransform.localPosition.z );
			}
			else {	
				objectTransform.localPosition = new Vector3( objectTransform.localPosition.x * 0.75f,
				                                             objectTransform.localPosition.y,
				                                             objectTransform.localPosition.z );
			}
		}
	}

	public void Update()
	{
		if (Application.isEditor && !Application.isPlaying)
		{	setSpread (this.transform);
		}
	}
}
