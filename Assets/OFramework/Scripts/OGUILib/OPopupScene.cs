﻿using UnityEngine;
using System.Collections;

public abstract class OPopupScene : OGameScene 
{
	public bool m_hasBackgroundShade = false;
	public Color m_backgroundShadeColor = new Color(0, 0, 0, 0.5f);
	protected GameObject m_backgroundShade = null;
	
	[HideInInspector]
	public SORTING_LAYER m_sortingLayer;
	
	public override void Awake () 
	{	this.gameObject.SetActive (false);

		OGameMgr.Instance.AddPopup (this);

		m_sortingLayer = SORTING_LAYER.POPUP;
		setSortingLayer(transform);
		
		InstantiateBackgroundShade();
	}
	
	protected void InstantiateBackgroundShade()
	{
		if (!m_hasBackgroundShade) return;
		
		m_backgroundShade = OFullscreenSprite.InstantiateFullscreenSprite("~BackgroundShade", m_backgroundShadeColor, SortingLayer.NameOf(m_sortingLayer), short.MinValue);
		m_backgroundShade.transform.parent = gameObject.transform;
	}
	
	public virtual void OnEnable()
	{	if(m_bannerAdPlacement == OBannerAdPlacement.Top)
		{	OInMobiHandler.Instance.createBannerAd();
		}	else if(m_bannerAdPlacement == OBannerAdPlacement.Bottom)
		{	OInMobiHandler.Instance.createBottomBannerAd();
		}
	}
	
	public virtual void OnDisable()
	{	if(m_bannerAdPlacement == OBannerAdPlacement.Top || 
		     m_bannerAdPlacement == OBannerAdPlacement.Bottom)
		{	OInMobiHandler.Instance.removeBannerAd();
		}
	}

	public void setSortingLayer(Transform obj)
	{	
		setSortingLayerObj(obj);

		for(int i=0; i< obj.childCount; i++)
		{	Transform objChild = obj.GetChild(i);

			setSortingLayerObj(objChild);
			
			if(objChild.childCount > 0)
			{	setSortingLayer(objChild);
			}
		}
	}

	void setSortingLayerObj(Transform obj)
	{
		if(obj.renderer != null)
		{	
			if(obj.GetComponent<OLabel>() != null)
			{	obj.GetComponent<OLabel>().m_fontSortingLayer = m_sortingLayer;
			}
			else
			{	switch(m_sortingLayer)
				{	case SORTING_LAYER.DEFAULT :
						obj.renderer.sortingLayerName = "Default";
						break;
					case SORTING_LAYER.UI :
						obj.renderer.sortingLayerName = "UI";
						break;
					case SORTING_LAYER.POPUP :
						obj.renderer.sortingLayerName = "Popup";
						break;
					case SORTING_LAYER.NOTIFICATION :
						obj.renderer.sortingLayerName = "Notification";
					break;
				}
			}
		}
	}
}
