using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;

public class OShareHandler : Singleton<OShareHandler> 
{
	#if UNITY_IPHONE
	[DllImport ("__Internal")]
	private static extern bool _IsFacebookAvailable();
	[DllImport ("__Internal")]
	private static extern void _FacebookShare(string msg);
	[DllImport ("__Internal")]
	private static extern void _FacebookShareWithPhoto(string msg, string photoFilename);
	[DllImport ("__Internal")]
	private static extern void _FacebookShareWithLink(string msg, string link);
	[DllImport ("__Internal")]
	private static extern void _FacebookShareWithPhotoAndLink(string msg, string photoFilename, string link);		
	[DllImport ("__Internal")]
	private static extern bool _IsTwitterAvailable();
	[DllImport ("__Internal")]
	private static extern void _Tweet(string msg);
	[DllImport ("__Internal")]
	private static extern void _TweetWithPhoto(string msg, string photoFilename);
	[DllImport ("__Internal")]
	private static extern void _TweetWithLink(string msg, string link);
	[DllImport ("__Internal")]
	private static extern void _TweetWithPhotoAndLink(string msg, string photoFilename, string link);
	#endif
	
	#if UNITY_ANDROID
	private static AndroidJavaObject jo;
	#endif
	
	public void InitMgr()
	{
		if(!Application.isEditor)
		{
			#if UNITY_ANDROID
			jo = new AndroidJavaObject("com.orangenose.nativeandroid.Share");
			#endif
		}	else
		{	Debug.Log("OShareHandler:: Cannot share in Editor.");
		}
	}
	
	public bool isFacebookAvailable()
	{	
		if(!Application.isEditor)
		{
			#if UNITY_IPHONE
			return _IsFacebookAvailable();
			#endif

			#if UNITY_ANDROID
			return (bool) jo.Call<bool>("isFacebookAvailable");
			#endif
		}	else
		{	Debug.Log("OShareHandler:: Cannot share in Editor.");
			return false;
		}

		return false;
	}

	public void facebookShare(string msg)
	{
		if(!Application.isEditor)
		{
			#if UNITY_IPHONE
			_FacebookShare(msg);
			#endif

			#if UNITY_ANDROID
			jo.Call("facebookShare", msg);
			#endif
		}	else
		{	Debug.Log("OShareHandler:: Cannot share in Editor.");
		}
	}

	public void facebookSharePhoto(string msg, string photoFilename)
	{
		if(!Application.isEditor)
		{
			#if UNITY_IPHONE
			_FacebookShareWithPhoto(msg, Application.persistentDataPath + "/" + photoFilename);
			#endif

			#if UNITY_ANDROID
			jo.Call("facebookSharePhoto", msg, Application.persistentDataPath + "/" + photoFilename);
			#endif
		}	else
		{	Debug.Log("OShareHandler:: Cannot share in Editor.");
		}
	}

	public void facebookShareLink(string msg, string link)
	{
		if(!Application.isEditor)
		{
			#if UNITY_IPHONE
			_FacebookShareWithLink(msg, link);
			#endif

			#if UNITY_ANDROID
			jo.Call("facebookShare", msg + "\n" + link);
			#endif
		}	else
		{	Debug.Log("OShareHandler:: Cannot share in Editor.");
		}
	}
	
	public void facebookSharePhotoAndLink(string msg, string photoFilename, string link)
	{
		if(!Application.isEditor)
		{
			#if UNITY_IPHONE
			_FacebookShareWithPhotoAndLink(msg, Application.persistentDataPath + "/" + photoFilename, link);
			#endif

			#if UNITY_ANDROID
			jo.Call("facebookSharePhoto", msg + "\n" + link, Application.persistentDataPath + "/" + photoFilename);
			#endif
		}	else
		{	Debug.Log("OShareHandler:: Cannot share in Editor.");
		}
	}

	public bool isTwitterAvailable()
	{	
		if(!Application.isEditor)
		{
			#if UNITY_IPHONE
			return _IsTwitterAvailable();
			#endif

			#if UNITY_ANDROID
			return (bool) jo.Call<bool>("isTwitterAvailable");
			#endif
		}	else
		{	Debug.Log("OShareHandler:: Cannot share in Editor.");
			return false;
		}

		return false;
	}
	
	public void tweet(string msg)
	{
		if(!Application.isEditor)
		{
			#if UNITY_IPHONE
			_Tweet(msg);
			#endif

			#if UNITY_ANDROID
			jo.Call("tweet", msg);
			#endif
		}	else
		{	Debug.Log("OShareHandler:: Cannot share in Editor.");
		}
	}

	public void tweetPhoto(string msg, string photoFilename)
	{
		if(!Application.isEditor)
		{	
			#if UNITY_IPHONE
			_TweetWithPhoto(msg, Application.persistentDataPath + "/" + photoFilename);
			#endif

			#if UNITY_ANDROID
			jo.Call("tweetPhoto", msg, Application.persistentDataPath + "/" + photoFilename);
			#endif
		}	else
		{	Debug.Log("OShareHandler:: Cannot share in Editor.");
		}
	}

	public void tweetLink(string msg, string link)
	{
		if(!Application.isEditor)
		{
			#if UNITY_IPHONE
			_Tweet(msg + " " + link);
			#endif

			#if UNITY_ANDROID
			jo.Call("tweet", msg + " " + link);
			#endif
		}	else
		{	Debug.Log("OShareHandler:: Cannot share in Editor.");
		}
	}

	public void tweetPhotoAndLink(string msg, string photoFilename, string link)
	{
		if(!Application.isEditor)
		{
			#if UNITY_IPHONE
			_TweetWithPhoto(msg + " " + link, Application.persistentDataPath + "/" + photoFilename);
			#endif

			#if UNITY_ANDROID
			jo.Call("tweetPhoto", msg + " " + link, Application.persistentDataPath + "/" + photoFilename);
			#endif
		}	else
		{	Debug.Log("OShareHandler:: Cannot share in Editor.");
		}
	}
}	
