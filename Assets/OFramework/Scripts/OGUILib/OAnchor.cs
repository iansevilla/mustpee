﻿using UnityEngine;
using System.Collections;

public enum AnchorX
{	None,
	Center,
	Left, 
	Right,
};

public enum AnchorY
{	None,
	Center,
	Top, 
	Bottom,
};

[ExecuteInEditMode]
public class OAnchor : MonoBehaviour 
{
	public AnchorX m_anchorX = AnchorX.Center;
	public AnchorY m_anchorY = AnchorY.Center;
	public Vector2 m_offset;

	public Transform m_referenceTransform;
	
	void Start () 
	{	
		setAnchor();
	}
	
	public void setAnchor()
	{	
		if(this.gameObject.renderer == null )
		{
			setAnchorNoSprite(this.transform, m_anchorX, m_anchorY, m_offset.x * 0.02f, m_offset.y * 0.02f);
		}
		else if(m_referenceTransform)
		{	
			setAnchor(this.transform, m_anchorX, m_anchorY, m_offset.x * 0.02f, m_offset.y * 0.02f, m_referenceTransform);
		}	
		else
		{	
			setAnchor(this.transform, m_anchorX, m_anchorY, m_offset.x * 0.02f, m_offset.y * 0.02f);
		}
	}

	public static void setAnchorNoSprite(Transform objectTransform, AnchorX anchorX, AnchorY anchorY, float xPos, float yPos)
	{
		if(anchorX == AnchorX.Center) 
		{	objectTransform.position = new Vector3(OResolutionHandler.C().x+xPos, objectTransform.position.y, objectTransform.position.z);
		}	else if (anchorX == AnchorX.Left) 
		{	objectTransform.position = new Vector3(OResolutionHandler.L()+xPos, objectTransform.position.y, objectTransform.position.z);
		}	else if (anchorX == AnchorX.Right) 
		{	objectTransform.position = new Vector3(OResolutionHandler.R()+xPos, objectTransform.position.y, objectTransform.position.z);
		}
		
		if (anchorY == AnchorY.Center) 
		{	objectTransform.position = new Vector3(objectTransform.position.x,  OResolutionHandler.C().y+yPos, objectTransform.position.z);
		} 	else if (anchorY == AnchorY.Top) 
		{	objectTransform.position = new Vector3(objectTransform.position.x,  OResolutionHandler.T()+yPos, objectTransform.position.z);
		}	else if (anchorY == AnchorY.Bottom) 
		{	objectTransform.position = new Vector3(objectTransform.position.x,  OResolutionHandler.B()+yPos, objectTransform.position.z);
		}
	}

	public static void setAnchor(Transform objectTransform, AnchorX anchorX, AnchorY anchorY, float xPos, float yPos)
	{
		if(anchorX == AnchorX.Center) 
		{	objectTransform.position = new Vector3(OResolutionHandler.C().x+xPos, objectTransform.position.y, objectTransform.position.z);
		}	else if (anchorX == AnchorX.Left) 
		{	objectTransform.position = new Vector3(OResolutionHandler.L()+xPos+OResolutionHandler.SpriteE(objectTransform).x, objectTransform.position.y, objectTransform.position.z);
		}	else if (anchorX == AnchorX.Right) 
		{	objectTransform.position = new Vector3(OResolutionHandler.R()+xPos-OResolutionHandler.SpriteE(objectTransform).x, objectTransform.position.y, objectTransform.position.z);
		}
		
		if (anchorY == AnchorY.Center) 
		{	objectTransform.position = new Vector3(objectTransform.position.x,  OResolutionHandler.C().y+yPos, objectTransform.position.z);
		} 	else if (anchorY == AnchorY.Top) 
		{	objectTransform.position = new Vector3(objectTransform.position.x,  OResolutionHandler.T()+yPos-OResolutionHandler.SpriteE(objectTransform).y, objectTransform.position.z);
		}	else if (anchorY == AnchorY.Bottom) 
		{	objectTransform.position = new Vector3(objectTransform.position.x,  OResolutionHandler.B()+yPos+OResolutionHandler.SpriteE(objectTransform).y, objectTransform.position.z);
		}
	}

	public static void setAnchor(Transform objectTransform, AnchorX anchorX, AnchorY anchorY, float xPos, float yPos, Transform referenceTransform)
	{
		if(anchorX == AnchorX.Center) 
		{	objectTransform.position = new Vector3(referenceTransform.position.x+xPos, objectTransform.position.y, objectTransform.position.z);
		}	else if (anchorX == AnchorX.Left) 
		{	objectTransform.position = new Vector3(referenceTransform.position.x-OResolutionHandler.SpriteE(referenceTransform).x+xPos+OResolutionHandler.SpriteE(objectTransform).x, objectTransform.position.y, objectTransform.position.z);
		}	else if (anchorX == AnchorX.Right) 
		{	objectTransform.position = new Vector3(referenceTransform.position.x+OResolutionHandler.SpriteE(referenceTransform).x+xPos-OResolutionHandler.SpriteE(objectTransform).x, objectTransform.position.y, objectTransform.position.z);
		}
		
		if (anchorY == AnchorY.Center) 
		{	objectTransform.position = new Vector3(objectTransform.position.x,  referenceTransform.position.y+yPos, objectTransform.position.z);
		} 	else if (anchorY == AnchorY.Top) 
		{	objectTransform.position = new Vector3(objectTransform.position.x,  referenceTransform.position.y+OResolutionHandler.SpriteE(referenceTransform).y+yPos-OResolutionHandler.SpriteE(objectTransform).y, objectTransform.position.z);
		}	else if (anchorY == AnchorY.Bottom) 
		{	objectTransform.position = new Vector3(objectTransform.position.x,  referenceTransform.position.y-OResolutionHandler.SpriteE(referenceTransform).y+yPos+OResolutionHandler.SpriteE(objectTransform).y, objectTransform.position.z);
		}
	}
	
	public void Update()
	{
		if (Application.isEditor && !Application.isPlaying)
		{	setAnchor();
		}
	}
}
