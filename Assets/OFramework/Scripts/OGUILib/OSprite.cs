﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (SpriteRenderer))]
[ExecuteInEditMode]
public class OSprite : MonoBehaviour {

	public SORTING_LAYER    	  m_sortingLayer = SORTING_LAYER.DEFAULT;
	public int    	  			  m_sortingOrder = 0;

	// Use this for initialization
	void Awake () {
		Init ();
	}

	void Init()
	{
		switch(m_sortingLayer)
		{
		case SORTING_LAYER.DEFAULT :
			this.renderer.sortingLayerName = "Default";
			break;
		case SORTING_LAYER.UI :
			this.renderer.sortingLayerName = "UI";
			break;
		case SORTING_LAYER.POPUP :
			this.renderer.sortingLayerName = "Popup";
			break;
		case SORTING_LAYER.NOTIFICATION :
			this.renderer.sortingLayerName = "Notification";
			break;
		}

		this.renderer.sortingOrder = m_sortingOrder;
	}

	public void Update() 
	{	
		if (Application.isEditor && !Application.isPlaying)
		{	Init();
		}
	}
}
