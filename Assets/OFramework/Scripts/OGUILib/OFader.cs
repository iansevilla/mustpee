﻿using UnityEngine;
using System.Collections;

public class OFader : MonoBehaviour {

	public virtual void Awake()
	{
		gameObject.SetActive(false);
	}
	
	public static OFader InstantiateFader(string name = "OFader")
	{
		GameObject go = OFullscreenSprite.InstantiateFullscreenSprite(name, Color.white, "Notification", short.MaxValue);
		
		OFader fader = go.AddComponent<OFader>();
		
		return fader;
	}
	
	public void Fade(Color color, float alphaBegin, float alphaEnd, float duration, LeanTweenType fadeEase = LeanTweenType.linear, bool deactivateOnFadeEnd = false, System.Action onFadeEnd = null)
	{
		gameObject.SetActive(true);
		
		SpriteRenderer sr = gameObject.GetComponent<SpriteRenderer>();
		
		sr.color = new Color(color.r, color.g, color.b, alphaBegin);
		
		LTDescr tween = LeanTween.alpha(gameObject, alphaEnd, duration).setEase(fadeEase);
		
		System.Action doOnFadeEnd = delegate {};
	
		if (onFadeEnd != null)
			doOnFadeEnd += onFadeEnd;
			
		if (deactivateOnFadeEnd)
			doOnFadeEnd += delegate { gameObject.SetActive(false); };
		
		tween.setOnComplete (doOnFadeEnd);
	}
}
