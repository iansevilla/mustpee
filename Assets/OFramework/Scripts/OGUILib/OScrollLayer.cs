﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

enum ScrollStates{
	IDLE,
	START,
	ONGOING,
	DONE
}

[RequireComponent (typeof(Collider2D))]
public class OScrollLayer : MonoBehaviour {
	public float      m_maxY;
	public float      m_rubberBandSpeed = 0.1f;
	public float      m_scrollDistAllowance = 1.5f;

	private ScrollStates m_curState;

	private Vector3   m_MaxYposition;
	private Vector3   m_MinYposition;
	private Vector2   m_currentPosition;
	private Vector3   m_currentObjectPosition;

	void Start(){
		m_curState = ScrollStates.IDLE;
		this.collider2D.enabled = false;

		this.m_MaxYposition = this.transform.localPosition + new Vector3( 0, 0, 0 );
		this.m_MinYposition = new Vector3 (0, m_maxY, 0);
	}

	void LateUpdate(){
		switch (m_curState) {
		case ScrollStates.IDLE :
			this.collider2D.enabled = false;

			if (this.transform.localPosition.y < this.m_MaxYposition.y)
				this.transform.Translate (new Vector3 (0, m_rubberBandSpeed, 0));
			
			if (this.transform.localPosition.y > this.m_MinYposition.y)
				this.transform.Translate (new Vector3 (0, -m_rubberBandSpeed, 0));

			if( OInputMgr.Instance.isTouchDown() ) {
				m_currentPosition = OInputMgr.Instance.getTouchScreenPos();
				m_curState = ScrollStates.START;
			}
			break;
		case ScrollStates.START :
			if( OInputMgr.Instance.isTouch()  ) {
				if ( ( OInputMgr.Instance.isTouch() && m_currentPosition.y > OInputMgr.Instance.getTouchScreenPos().y + m_scrollDistAllowance ) ||
				    ( OInputMgr.Instance.isTouch() && m_currentPosition.y < OInputMgr.Instance.getTouchScreenPos().y - m_scrollDistAllowance ) )
				{
					m_currentObjectPosition = this.transform.localPosition;
					m_curState = ScrollStates.ONGOING;

					this.collider2D.enabled = true;
				}
			}
			if( OInputMgr.Instance.isTouchUp() ) {
				m_curState = ScrollStates.IDLE;
			}
			break;
		case ScrollStates.ONGOING :
			if( OInputMgr.Instance.isTouch ()) {
				Vector3 touchInputPosition = Camera.main.ScreenToWorldPoint( new Vector3( 0, OInputMgr.Instance.getTouchScreenPos().y, 0 ));
				Vector3 touchInputPositionAfter = Camera.main.ScreenToWorldPoint( new Vector3( 0, m_currentPosition.y, 0 ) );
				this.transform.localPosition = ( touchInputPosition - touchInputPositionAfter ) + m_currentObjectPosition;
			}
			if( OInputMgr.Instance.isTouchUp() ) {
				m_curState = ScrollStates.IDLE;
			}
			break;
		}
	}
}
