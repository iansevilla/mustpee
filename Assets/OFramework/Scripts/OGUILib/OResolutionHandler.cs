﻿using UnityEngine;
using System.Collections;

public class OResolutionHandler : MonoBehaviour 
{
	// Get Screen width
	public static float screenWidth()
	{	return Camera.main.pixelWidth;
	}

	// Get Screen width
	public static float screenHeight()
	{	return Camera.main.pixelHeight;
	}

	// Get the ScreenToWorldPoint of Left of Screen
	public static float L()
	{	return Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).x;
	}

	// Get the ScreenToWorldPoint of Right of Screen
	public static float R()
	{	return Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0, 0)).x;
	}

	// Get the ScreenToWorldPoint of Bottom of Screen
	public static float B()
	{	return Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).y;
	}

	// Get the ScreenToWorldPoint of Top of Screen
	public static float T()
	{	return Camera.main.ScreenToWorldPoint(new Vector3(0, Camera.main.pixelHeight, 0)).y;
	}

	// Get the ScreenToWorldPoint of Center of Screen
	public static Vector2 C()
	{	return Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth/2, Camera.main.pixelHeight/2, 0));
	}

	// Get the ScreenToWorldPoint of Size of Screen
	public static Vector2 S()
	{	return Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, Camera.main.pixelHeight, 0));
	}

	// get Extents (half of size) of the Sprite
	public static Vector2 SpriteE(Transform sprite)
	{	return sprite.renderer.bounds.extents;
	}
	
	public static Vector2 SpriteC(Transform sprite)
	{	return sprite.renderer.bounds.center;
	}
	
	public static Vector2 SpriteS(Transform sprite)
	{	return sprite.renderer.bounds.size;
	}
}
