﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(SpriteRenderer))]
public class OSpriteToggleButton : OSpriteButton 
{
	public bool m_isToggled = false;

	public override void OnDown()
	{	base.OnDown();	
		
		m_isToggled = !m_isToggled;
		Toggle(m_isToggled);
	}

	public override void OnUp() 
	{
	}

	public void Toggle(bool isToggled)
	{	
		if(isToggled)
		{	if(m_buttonDownSprite != null)
			{	gameObject.GetComponent<SpriteRenderer>().sprite = m_buttonNormalSprite;
			}	else
			{	gameObject.GetComponent<SpriteRenderer>().color = m_buttonColor;
			}
		}	else
		{	if(m_buttonDownSprite != null)
			{	gameObject.GetComponent<SpriteRenderer>().sprite = m_buttonDownSprite;
			}	else
			{	gameObject.GetComponent<SpriteRenderer>().color = new Color( 0.7f, 0.7f, 0.7f);
			}
		}

		m_isToggled = isToggled;
	}
}