﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class OGameScene : MonoBehaviour 
{
	[HideInInspector]
	public bool m_isAcceptTouch = true;
	[HideInInspector]
	public List<OButton> m_buttons;	
	public bool m_isPauseOnInterrupt = false;

	public OBannerAdPlacement m_bannerAdPlacement; 
	
	public virtual void Awake() 
	{	
		OCameraMgr.SetupCamera ();
		OGameMgr.Instance.SetCurrentGameScene (this);

		if(m_bannerAdPlacement == OBannerAdPlacement.Top)
		{	OInMobiHandler.Instance.createBannerAd();
		}	else if(m_bannerAdPlacement == OBannerAdPlacement.Bottom)
		{	OInMobiHandler.Instance.createBottomBannerAd();
		}
	}

	public virtual void OnDestroy() 
	{	
		if(m_bannerAdPlacement == OBannerAdPlacement.Top || 
		   m_bannerAdPlacement == OBannerAdPlacement.Bottom)
		{	OInMobiHandler.Instance.removeBannerAd();
		}
	}
	
	public virtual void OnTouchDown()
	{
		for(int i=0; i< m_buttons.Count; i++)
		{	if(m_buttons[i].isTouched())
			{	m_buttons[i].OnDown();
				OnButtonDown(m_buttons[i]);	
			}
		}
	}

	public virtual void OnTouchUp()
	{	
		for(int i=0; i< m_buttons.Count; i++)
		{	if(m_buttons[i].isTouched() && !m_buttons[i].isDisabled)
			{	m_buttons[i].OnUp();
				OnButtonUp(m_buttons[i]);	
			}
		}
	}

	public virtual void OnTouchDown(Vector3 pos)
	{
		for(int i=0; i< m_buttons.Count; i++)
		{	if(m_buttons[i].collider2D.OverlapPoint(pos) && !m_buttons[i].isDisabled)
			{	m_buttons[i].OnDown();
				OnButtonDown(m_buttons[i]);	
			}
		}
	}

	public virtual void OnTouchUp(Vector3 pos)
	{	
		for(int i=0; i< m_buttons.Count; i++)
		{	if(m_buttons[i].collider2D.OverlapPoint(pos) && !m_buttons[i].isDisabled)
			{	m_buttons[i].OnUp();
				OnButtonUp(m_buttons[i]);	
			}
		}
	}

	public virtual void OnButtonDown(OButton button){} 
	public virtual void OnButtonUp(OButton button){} 

	public virtual void OnKeycodeEscape()
	{
		for(int i=0; i< m_buttons.Count; i++)
		{	OButton button = m_buttons[i];
			if(button.isAndroidBackButton)
			{	m_buttons[i].OnUp();
				OnButtonUp(m_buttons[i]);	
			}
		}
	}
	
	public virtual void SetScenePaused(bool paused)
	{
		if (paused)
		{
			Time.timeScale = 0;
			
			OnScenePause (true);
		} else {
			Time.timeScale = 1;
			
			OnScenePause(false);
		}
	}
	
	public virtual void OnScenePause(bool paused) { }
}
