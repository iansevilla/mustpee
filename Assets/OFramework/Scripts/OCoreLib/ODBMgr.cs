﻿using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;

public class ODBMgr : Singleton<ODBMgr> 
{
	private int m_dbAppVer;
	
	public void InitMgr()
	{	
		m_dbAppVer =  OGameMgr.Instance.APP_VER;		
		setInt(ODBKeys.APP_VER, m_dbAppVer);

		initDBValues();
		ODBKeys.patchKeys(m_dbAppVer);
	}
	
	void initDBValues()
	{	
		Hashtable dbValues = ODBKeys.getInitDBValues();
		
		foreach (string key in dbValues.Keys) 
		{	
			if(dbValues[key].GetType() == typeof(int))
			{	initString(key, ((int) dbValues[key]).ToString(), false);
			}	else if(dbValues[key].GetType() == typeof(float))
			{	initString(key, ((float) dbValues[key]).ToString(), false);
			}	else if(dbValues[key].GetType() == typeof(bool))
			{	initString(key, ((bool) dbValues[key]).ToString(), false);
			}	else if(dbValues[key].GetType() == typeof(DateTime))
			{	initString(key, ((DateTime) dbValues[key]).ToString(), false);
			}	else
			{	initString(key, (string) dbValues[key], false);
			}
		}
		
		sync();
	}
	
	public bool hasKey(string key)
	{	return PlayerPrefs.HasKey(key);
	}
	
	void initString(string key, string value)
	{
		initString (key, value, true);
	}
	
	void initString(string key, string value, bool syncImmediately)
	{
		// don't override existing keys
		if(!PlayerPrefs.HasKey(key))
		{	PlayerPrefs.SetString (key, value);
		}
		
		if (syncImmediately) 
		{	sync ();
		} 	else 
		{	// ATTENTION Don't forget to sync DB
		}
	}
	
	public string getString(string key)
	{	
		if (PlayerPrefs.HasKey (key)) 
		{	return PlayerPrefs.GetString(key);
		}
		
		Debug.LogWarning("ODBMgr :: Failed to getString for " + key + ". Key doesn't exist.");
		
		return "";
	}
	
	public string getString(string key, string defaultValue)
	{	
		if (PlayerPrefs.HasKey (key)) 
		{	return PlayerPrefs.GetString(key);
		}

		Debug.LogWarning("ODBMgr :: Failed to getString for " + key + ". Key doesn't exist. Returned defaultValue " + defaultValue + ".");
		
		return defaultValue;
	}
	
	public void setString(string key, string value)
	{	setString(key, value, true);
	}
	
	public void setString(string key, string value, bool syncImmediately)
	{	
		PlayerPrefs.SetString(key, value);
		
		if (syncImmediately) 
		{	sync();
		}	else
		{	// ATTENTION Don't forget to sync DB
		}
	}
	
	public int getInt(string key)
	{	
		if (PlayerPrefs.HasKey(key)) 
		{		
			string value = PlayerPrefs.GetString(key);
			
			int intValue;
			
			if(int.TryParse(value, out intValue))
			{	return intValue;
			}
			
			string errorMsg = "ODBMgr :: Failed to getInt for " + key + ". Parsing failed.";			
			Debug.LogError(errorMsg);
			OGAHandler.Instance.SendCrash(errorMsg, false);
			
			return 0;
		}
		
		Debug.LogWarning("ODBMgr :: Failed to getInt for " + key + ". Key doesn't exist.");
		
		return 0;
	}
	
	public int getInt(string key, int defaultValue)
	{
		if (PlayerPrefs.HasKey(key)) 
		{		
			string value = PlayerPrefs.GetString(key);
			
			int intValue;
			
			if(int.TryParse(value, out intValue))
			{	return intValue;
			}

			string errorMsg = "ODBMgr :: Failed to getInt for " + key + ". Parsing failed. Returned defaultValue " + defaultValue + ".";			
			Debug.LogError(errorMsg);
			OGAHandler.Instance.SendCrash(errorMsg, false);

			return defaultValue;
		}
		
		Debug.LogWarning("ODBMgr :: Failed to getInt for " + key + ". Key doesn't exist. Returned defaultValue " + defaultValue + ".");

		return defaultValue;
	}
	
	public void setInt(string key, int value)
	{	setInt(key, value, true);
	}
	
	public void setInt(string key, int value, bool syncImmediately)
	{	
		PlayerPrefs.SetString(key, value.ToString());	
		
		if (syncImmediately) 
		{	sync();		
		}	else
		{	// ATTENTION Don't forget to sync DB
		}
	}
	
	public float getFloat(string key)
	{	
		if (PlayerPrefs.HasKey (key)) 
		{	
			string value = PlayerPrefs.GetString(key);
			
			float floatValue;
			
			if(float.TryParse(value, out floatValue))
			{	return floatValue;
			}
			
			string errorMsg = "ODBMgr :: Failed to getFloat for " + key + ". Parsing failed.";			
			Debug.LogError(errorMsg);
			OGAHandler.Instance.SendCrash(errorMsg, false);

			return 0;
		}
		
		Debug.LogWarning("ODBMgr :: Failed to getFloat for " + key + ". Key doesn't exist.");
		
		return 0;
	}
	
	public float getFloat(string key, float defaultValue)
	{
		if (PlayerPrefs.HasKey(key)) 
		{		
			string value = PlayerPrefs.GetString(key);
			
			float floatValue;
			
			if(float.TryParse(value, out floatValue))
			{	return floatValue;
			}

			string errorMsg = "ODBMgr :: Failed to getFloat for " + key + ". Parsing failed. Returned defaultValue " + defaultValue + ".";			
			Debug.LogError(errorMsg);
			OGAHandler.Instance.SendCrash(errorMsg, false);

			return defaultValue;
		}

		Debug.LogWarning("ODBMgr :: Failed to getFloat for " + key + ". Key doesn't exist. Returned defaultValue " + defaultValue + ".");
		
		return defaultValue;
	}
	
	public void setFloat(string key, float value)
	{	setFloat (key, value, true);
	}
	
	public void setFloat(string key, float value, bool syncImmediately)
	{	
		PlayerPrefs.SetString(key, value.ToString());
		
		if (syncImmediately) 
		{	sync();	
		}	else
		{	// ATTENTION Don't forget to sync DB
		}
	}
	
	public bool getBool(string key)
	{	
		if (PlayerPrefs.HasKey (key)) 
		{	
			string value = PlayerPrefs.GetString(key);
			
			bool boolValue;
			
			if(bool.TryParse(value, out boolValue))
			{	return boolValue;
			}
			
			string errorMsg = "ODBMgr :: Failed to getBool for " + key + ". Parsing failed.";			
			Debug.LogError(errorMsg);
			OGAHandler.Instance.SendCrash(errorMsg, false);
			
			return false;
		}
		
		Debug.LogWarning("ODBMgr :: Failed to getBool for " + key + ". Key doesn't exist.");
		
		return false;
		
	}
	
	public bool getBool(string key, bool defaultValue)
	{
		if (PlayerPrefs.HasKey(key)) 
		{		
			string value = PlayerPrefs.GetString(key);
			
			bool boolValue;
			
			if(bool.TryParse(value, out boolValue))
			{	return boolValue;
			}
			
			string errorMsg = "ODBMgr :: Failed to getBool for " + key + ". Parsing failed. Returned defaultValue " + defaultValue + ".";			
			Debug.LogError(errorMsg);
			OGAHandler.Instance.SendCrash(errorMsg, false);	

			return defaultValue;
		}
		
		Debug.LogWarning("ODBMgr :: Failed to getBool for " + key + ". Key doesn't exist. Returned defaultValue " + defaultValue + ".");
		
		return defaultValue;
	}
	
	public void setBool(string key, bool value)
	{	setBool(key, value, true);
	}
	
	public void setBool(string key, bool value, bool syncImmediately)
	{	
		PlayerPrefs.SetString(key, value.ToString());
		
		if (syncImmediately) 
		{	sync();
		}	else
		{	// ATTENTION Don't forget to sync DB
		}
	}
	
	public DateTime getDateTime(string key)
	{	
		if (PlayerPrefs.HasKey (key)) 
		{	
			string value = PlayerPrefs.GetString(key);
			
			DateTime dateTimeValue;
			
			if(DateTime.TryParse(value, out dateTimeValue))
			{	return dateTimeValue;
			}
			
			string errorMsg = "ODBMgr :: Failed to getDateTime for " + key + ". Parsing failed.";			
			Debug.LogError(errorMsg);
			OGAHandler.Instance.SendCrash(errorMsg, false);	

			return DateTime.Now;
		}
		
		Debug.LogWarning("ODBMgr :: Failed to getDateTime for " + key + ". Key doesn't exist.");
		
		return DateTime.Now;
	}
	
	public DateTime getDateTime(string key, DateTime defaultValue)
	{
		if (PlayerPrefs.HasKey(key)) 
		{		
			string value = PlayerPrefs.GetString(key);
			
			DateTime dateTimeValue;
			
			if(DateTime.TryParse(value, out dateTimeValue))
			{	return dateTimeValue;
			}
		
			string errorMsg = "ODBMgr :: Failed to getDateTime for " + key + ". Parsing failed. Returned defaultValue " + defaultValue + ".";			
			Debug.LogError(errorMsg);
			OGAHandler.Instance.SendCrash(errorMsg, false);			

			return defaultValue;
		}
		
		Debug.LogWarning("ODBMgr :: Failed to getDateTime for " + key + ". Key doesn't exist. Returned defaultValue " + defaultValue + ".");

		return defaultValue;
	}
	
	public void setDateTime(string key, DateTime value)
	{	setDateTime(key, value, true);
	}
	
	public void setDateTime(string key, DateTime value, bool syncImmediately)
	{	
		PlayerPrefs.SetString(key, value.ToString());
		
		if (syncImmediately) 
		{	sync();
		}	else
		{	// ATTENTION Don't forget to sync DB
		}
	}
	
	public void sync()
	{	PlayerPrefs.Save();
	}
}
