﻿using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;

public class OGAHandler : Singleton<OGAHandler> 
{
	string m_sendOnceEvents;
	
	#if UNITY_IPHONE
	[DllImport ("__Internal")]
	private static extern void _SetGAOptOut(bool optOut);
	[DllImport ("__Internal")]
	private static extern void _SetGADryRun(bool dryRun);
	[DllImport ("__Internal")]
	private static extern void _SetGADispatchInterval(float interval);
	[DllImport ("__Internal")]
	private static extern void _GADispatch();
	[DllImport ("__Internal")]
	private static extern void _SetGAUserID(string userID, string analyticsID);
	[DllImport ("__Internal")]
	private static extern void _SendGAScreen(string screenName, string analyticsID);
	[DllImport ("__Internal")]
	private static extern void _SendGAEvent(string category, string action, string label, float value, string analyticsID);
	[DllImport ("__Internal")]
	private static extern void _SendGATimedEvent(string category, float interval, string name, string label, string analyticsID);
	[DllImport ("__Internal")]
	private static extern void _SendGASocialInteraction(string socialNetwork, string action, string target, string analyticsID);
	[DllImport ("__Internal")]
	private static extern void _SendGAPurchaseTransaction(string transactionID, string name, string sku, string category, float price, int quantity, string currency, string analyticsID);
	[DllImport ("__Internal")]
	private static extern void _SendGACrash(string description, bool isFatal, string analyticsID);
	#endif
	
	#if UNITY_ANDROID
	private static AndroidJavaObject jo;
	#endif

	public void InitMgr()
	{	
		if (!Application.isEditor)
		{	
			#if UNITY_ANDROID
			jo = new AndroidJavaObject("com.orangenose.googleplayservice.GoogleAnalyticsPlugin");
			#endif
	
		}	else
		{	Debug.Log("OGAHandler:: Google Analytics does not run in Editor.");
		}
		
		m_sendOnceEvents = ODBMgr.Instance.getString(ODBKeys.ANALYTICS_SEND_ONCE_EVENTS, "");
		SetOptOut(ODBMgr.Instance.getBool(ODBKeys.ANALYTICS_OPT_OUT));		
	}

	// ATTENTION
	//	You can enable an app-level opt out flag that will disable Google Analytics across the entire app. 
	public void SetOptOut(bool optOut)
	{
		ODBMgr.Instance.setBool(ODBKeys.ANALYTICS_OPT_OUT, optOut);
		
		if (!Application.isEditor)
		{	
			#if UNITY_IPHONE
			_SetGAOptOut(optOut);
			#endif
		
			#if UNITY_ANDROID
			jo.Call("SetOptOut", optOut);
			#endif
		}	else
		{	Debug.Log("OGAHandler:: Google Analytics does not run in Editor.");
		}
	}

	// ATTENTION
	// The SDK provides a dryRun flag that when set, prevents any data from being sent to Google Analytics. 
	// The dryRun flag should be set whenever you are testing or debugging an implementation and do not want test data to appear in your Google Analytics reports.
	public void SetDryRun(bool dryRun)
	{
		if (!Application.isEditor)
		{	
			#if UNITY_IPHONE
			_SetGADryRun(dryRun);
			#endif

			#if UNITY_ANDROID
			jo.Call("SetDryRun", dryRun);
			#endif
		}	else
		{	Debug.Log("OGAHandler:: Google Analytics does not run in Editor.");
		}
	}

	// ATTENTION
	// This sets how often data is dispatched from the SDK.
	// By default, data is dispatched from the SDK for iOS every 2 minutes.
	// Setting a negative value will disable periodic dispatch, requiring that you use manual dispatch if you want to send any data to Google Analytics.
	public void SetDispatchInterval(float interval)
	{
		if (!Application.isEditor)
		{	
			#if UNITY_IPHONE
			_SetGADispatchInterval(interval);
			#endif

			#if UNITY_ANDROID
			jo.Call("SetDispatchInterval", interval);
			#endif
		}
	}
		
	// ATTENTION
	// To manually dispatch hits, for example when you know the device radio is already being used to send other data.
	public void Dispatch()
	{
		if (!Application.isEditor)
		{	
			#if UNITY_IPHONE
			_GADispatch();
			#endif

			#if UNITY_ANDROID
			jo.Call("Dispatch");
			#endif
		}	else
		{	Debug.Log("OGAHandler:: Google Analytics does not run in Editor.");
		}
	}

	public void SetUserID(string userID)
	{
		if (!Application.isEditor)
		{
			Debug.Log("OGAHandler:: SetUserID " + userID);
			
			#if UNITY_IPHONE
			_SetGAUserID(userID, OGameMgr.Instance.GA_IOS);
			#endif

			#if UNITY_ANDROID
			jo.Call("SetUserID", userID, OGameMgr.Instance.GA_ANDROID);
			#endif
		}
	}
	
	// ATTENTION
	// Field Name 		Type		Required	Description
	// Screen Name		string		Yes			The name of an application screen (e.g. HomeScreen)
	public void SendScreen(string screenName)
	{
		if (!Application.isEditor)
		{
			Debug.Log("OGAHandler:: SendScreen " + screenName);
			
			#if UNITY_IPHONE
			_SendGAScreen(screenName, OGameMgr.Instance.GA_IOS);
			#endif

			#if UNITY_ANDROID
			jo.Call("SendScreen", screenName, OGameMgr.Instance.GA_ANDROID);
			#endif
		}	else
		{	Debug.Log("OGAHandler:: Google Analytics does not run in Editor.");
		}
	}

	public void SendScreenOnce(string screenName)
	{
		if(!m_sendOnceEvents.Contains(screenName))
		{	m_sendOnceEvents+= screenName + ",";
			ODBMgr.Instance.setString(ODBKeys.ANALYTICS_SEND_ONCE_EVENTS, m_sendOnceEvents);

			SendScreen(screenName+"Once");
		}			
	}

	// ATTENTION
	// Field Name 		Type		Required	Description
	// Category			string		Yes			The event category
	// Action			string		Yes			The event action
	// Label			string		No			The event label
	// Value			float		No			The event value
	public void SendEvent(string category, string action, string label, float value)
	{
		if (!Application.isEditor)
		{	
			Debug.Log("OGAHandler:: SendEvent " + action);
			
			#if UNITY_IPHONE
			_SendGAEvent(category, action, label, value, OGameMgr.Instance.GA_IOS);
			#endif

			#if UNITY_ANDROID
			jo.Call("SendEvent", category, action, label, value, OGameMgr.Instance.GA_ANDROID);
			#endif
		}	else
		{	Debug.Log("OGAHandler:: Google Analytics does not run in Editor.");
		}
	}

	public void SendEventOnce(string category, string action, string label, float value)
	{
		string keyName = category+action+label;

		if(!m_sendOnceEvents.Contains(keyName))
		{	m_sendOnceEvents+= keyName + ",";
			ODBMgr.Instance.setString(ODBKeys.ANALYTICS_SEND_ONCE_EVENTS, m_sendOnceEvents);
			
			SendEvent(category, action+"Once", label, value);
		}	
	}
	
	// ATTENTION
	// Field Name 		Type		Required	Description
	// Category			string		Yes			The category of the timed event
	// Value			float		Yes			The timing measurement in milliseconds
	// Name				string		No			The name of the timed event
	// Label			string		No			The label of the timed event
	public void SendTimedEvent(string category, float interval, string name, string label)
	{
		if (!Application.isEditor)
		{	
			Debug.Log("OGAHandler:: SendTimedEvent " + name);
		
			#if UNITY_IPHONE
			_SendGATimedEvent(category, interval, name, label, OGameMgr.Instance.GA_IOS);
			#endif

			#if UNITY_ANDROID
			jo.Call("SendTimedEvent", category, interval, name, label, OGameMgr.Instance.GA_ANDROID);
			#endif
		}	else
		{	Debug.Log("OGAHandler:: Google Analytics does not run in Editor.");
		}
	}
	
	public void SendTimedEventOnce(string category, float interval, string name, string label)
	{
		string keyName = category+name+label;
		
		if(!m_sendOnceEvents.Contains(keyName))
		{	m_sendOnceEvents+= keyName + ",";
			ODBMgr.Instance.setString(ODBKeys.ANALYTICS_SEND_ONCE_EVENTS, m_sendOnceEvents);
			
			SendTimedEvent(category, interval, name+"Once", label);
		}
	}

	// ATTENTION
	// Field Name 		Type		Required	Description
	// Social Network	string		Yes			The social network with which the user is interacting (e.g. Facebook, Google+, Twitter, etc.).
	// Social Action	string		Yes			The social action taken (e.g. Like, Share, +1, etc.).
	// Social Target	string		No			The content on which the social action is being taken (i.e. a specific article or video).
	public void SendSocialInteraction(string socialNetwork, string action, string target)
	{
		if (!Application.isEditor)
		{
			Debug.Log("OGAHandler:: SendSocialInteraction " + socialNetwork + " " + action);
			
			#if UNITY_IPHONE
			_SendGASocialInteraction(socialNetwork, action, target, OGameMgr.Instance.GA_IOS);
			#endif

			#if UNITY_ANDROID
			jo.Call("SendSocialInteraction", socialNetwork, action, target, OGameMgr.Instance.GA_ANDROID);
			#endif
		}	else
		{	Debug.Log("OGAHandler:: Google Analytics does not run in Editor.");
		}
	}

	public void SendSocialInteractionOnce(string socialNetwork, string action, string target)
	{
		string keyName = socialNetwork+action+target;

		if(!m_sendOnceEvents.Contains(keyName))
		{	m_sendOnceEvents+= keyName + ",";
			ODBMgr.Instance.setString(ODBKeys.ANALYTICS_SEND_ONCE_EVENTS, m_sendOnceEvents);
			
			SendSocialInteraction(socialNetwork, action+"Once", target);
		}	
	}
	
	// ATTENTION
	// No Ecommerce data appears in your reports until you set up Ecommerce tracking in Analytics Dashboard:
	// 1. Click Admin from the menu bar at the top of any screen in Analytics.
	// 2. Use the drop down menus to select the Account, Property, and View.
	// 3. Click View Settings.
	// 4. In the Ecommerce Settings section, click the toggle so it says ON
	// 5. Click Save at the bottom of the page.
	//
	// Field Name 		Type		Required	Description
	// Transaction ID	string		Yes			The transaction ID with which the item should be associated
	// Name				string		Yes			The name of the product
	// SKU				string		Yes			The SKU of a product
	// Category			string		No			A category to which the product belongs
	// Price			float		Yes			The price of a product
	// Quantity			int			Yes			The quantity of a product
	// Currency code	string		No			The local currency of a transaction. Defaults to the currency of the view (profile) in which the transactions are reported
	public void SendPurchaseTransaction(string transactionID, string name, string sku, string category, float price, int quantity, string currency)
	{	
		if (!Application.isEditor)
		{
			Debug.Log("OGAHandler:: SendPurchaseTransaction " + name);
			
			#if UNITY_IPHONE
			_SendGAPurchaseTransaction(transactionID, name, sku, category, price, quantity, currency, OGameMgr.Instance.GA_IOS);
			#endif

			#if UNITY_ANDROID
			jo.Call("SendPurchaseTransaction", transactionID, name, sku, category, price, quantity, currency, OGameMgr.Instance.GA_ANDROID);
			#endif
		}	else
		{	Debug.Log("OGAHandler:: Google Analytics does not run in Editor.");
		}
	}
	
	public void SendPurchaseTransactionOnce(string transactionID, string name, string sku, string category, float price, int quantity, string currency)
	{
		if(!m_sendOnceEvents.Contains(transactionID))
		{	m_sendOnceEvents+= transactionID + ",";
			ODBMgr.Instance.setString(ODBKeys.ANALYTICS_SEND_ONCE_EVENTS, m_sendOnceEvents);
			
			SendPurchaseTransaction(transactionID, name+"Once", sku, category, price, quantity, currency);
		}	
	}
	
	// ATTENTION
	// Field Name 		Type		Required	Description
	// Description		string		No			A description of the exception (up to 100 characters). Acceptsnil.
	// isFatal			bool		Yes			Indicates whether the exception was fatal. YES indicates fatal.
	public void SendCrash(string description, bool isFatal)
	{
		if (!Application.isEditor)
		{
			Debug.Log("OGAHandler:: SendCrash " + description);
			
			#if UNITY_IPHONE
			_SendGACrash(description, isFatal, OGameMgr.Instance.GA_IOS);
			#endif

			#if UNITY_ANDROID
			jo.Call("SendCrash", description, isFatal, OGameMgr.Instance.GA_ANDROID);
			#endif
		}	else
		{	Debug.Log("OGAHandler:: Google Analytics does not run in Editor.");
		}
	}
}
