﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;   

public class OTapJoyHandler : Singleton<OTapJoyHandler> 
{
	public void InitMgr()
	{
		if(!Application.isEditor)
		{
			#if UNITY_IPHONE
			TapjoyPluginIOS.RequestTapjoyConnect(OGameMgr.Instance.TAPJOY_IOS, OGameMgr.Instance.TAPJOY_APPSECRET_IOS);
			#endif
		
			#if UNITY_ANDROID
			TapjoyPluginAndroid.RequestTapjoyConnect(OGameMgr.Instance.TAPJOY_ANDROID, OGameMgr.Instance.TAPJOY_APPSECRET_ANDROID);
			#endif
		}
	}
}