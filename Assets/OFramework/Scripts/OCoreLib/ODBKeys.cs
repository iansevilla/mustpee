﻿using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;

public class ODBKeys
{
	// ATTENTION: ADD ALL DB KEYS HERE!

	public const string APP_VER = "APP_VER";
	public const string IS_APP_FIRST_OPEN = "IS_APP_FIRST_OPEN";

	// REMOTE CONFIG
	public const string REMOTE_CONFIG = "REMOTE_CONFIG";
	public const string REMOTE_DOWNLOAD_DATE = "REMOTE_DOWNLOAD_DATE";

	// ANALYTICS
	public const string ANALYTICS_SEND_ONCE_EVENTS = "ANALYTICS_SEND_ONCE_EVENTS";
	public const string ANALYTICS_OPT_OUT = "ANALYTICS_OPT_OUT";
	
	// RATING
	public const string RATING_DONE = "RATING_DONE";

	// FACEBOOK
	public const string FB_USER_ID = "FB_USER_ID";
	public const string FB_USER_NAME = "FB_USER_NAME";
	public const string FB_FRIENDS_ID = "FB_FRIENDS_ID";
	public const string FB_FRIENDS_NAME = "FB_FRIENDS_NAME";
	public const string FB_ACCESS_TOKEN = "FB_ACCESS_TOKEN";
	public const string FB_TOKEN_EXPIRATION = "FB_TOKEN_EXPIRATION";

	// KPI
	public const string KPI_TOTAL_TIME_SPENT = "KPI_TOTAL_TIME_SPENT";

	// GAME
	public const string GAME_BGM_ENABLED = "GAME_BGM_ENABLED";
	public const string GAME_SFX_ENABLED = "GAME_SFX_ENABLED";

	public static Hashtable getInitDBValues()
	{
		Hashtable dbValues = new Hashtable();

		// ATTENTION: INIT ALL KEYS AND VALUES HERE!
		
		dbValues.Add (IS_APP_FIRST_OPEN, true);
		dbValues.Add (APP_VER, 1);  

		// REMOTE CONFIG
		dbValues.Add(REMOTE_CONFIG, "");
		dbValues.Add(REMOTE_DOWNLOAD_DATE, DateTime.Now);

		// ANALYTICS 
		dbValues.Add(ANALYTICS_SEND_ONCE_EVENTS, "");
		dbValues.Add(ANALYTICS_OPT_OUT, false);

		// RATING
		dbValues.Add(RATING_DONE, false);

		// FACEBOOK
		dbValues.Add(FB_USER_ID, "");
		dbValues.Add(FB_USER_NAME, "");
		dbValues.Add(FB_FRIENDS_ID, "");
		dbValues.Add(FB_FRIENDS_NAME, "");
		dbValues.Add(FB_ACCESS_TOKEN, "");
		dbValues.Add(FB_TOKEN_EXPIRATION, DateTime.Now);

		// KPI 
		dbValues.Add(KPI_TOTAL_TIME_SPENT, 0f);

		// GAME
		dbValues.Add(GAME_BGM_ENABLED, true);
		dbValues.Add(GAME_SFX_ENABLED, true);

		return dbValues;
	}

	public static void patchKeys(int dbAppVer)
	{	
		int appVer = OGameMgr.Instance.APP_VER;		
		
		// ATTENTION: PATCH KEYS HERE!

		while(dbAppVer < appVer)
		{
			switch(dbAppVer)
			{
				case 0:	break;
			}

			dbAppVer++;
		}
	}
}
