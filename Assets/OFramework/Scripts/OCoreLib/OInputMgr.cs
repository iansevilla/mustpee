﻿/*
 * OInputMgr.cs
 * - Platform Agnostic Input Manager
 * 
 *   DAM
 */ 

using UnityEngine;
using System.Collections;

public class OInputMgr : Singleton<OInputMgr> 
{
	public int getTouchCount()
	{	
		if (!Application.isEditor )  
		{	return Input.touchCount;
		}	else
		{	if( Input.GetMouseButton(0) || Input.GetMouseButtonUp(0) || Input.GetMouseButtonDown(0)) 
			{   return 1;
			}
		}

		return 0;
	}

	public bool isTouch(int index)
	{
		if( !Application.isEditor )
		{	if( Input.GetTouch(index).phase != TouchPhase.Canceled ) 
			{	return true;
			}
		}
		else
		{	if( Input.GetMouseButton(0) )
			{	return true;
			}
		}
		
		return false;
	}
		
	public bool isTouch() 
	{
		if (!Application.isEditor )  
		{   
			foreach(Touch touch in Input.touches)
			{	if(touch.phase != TouchPhase.Canceled)
				{	return true;
				}
			}
		} 
		else 
		{   if( Input.GetMouseButton(0) ) 
			{   return true;
			}
		}
		
		return false;
	}

	public bool isTouchDown(int index)
	{
		if( !Application.isEditor )
		{	if( Input.GetTouch(index).phase == TouchPhase.Began ) 
			{	return true;
			}
		}
		else
		{	if( Input.GetMouseButtonDown(0) )
			{	return true;
			}
		}

		return false;
	}

	public bool isTouchDown() 
	{
		if (!Application.isEditor )  
		{   foreach(Touch touch in Input.touches)
			{	if(touch.phase == TouchPhase.Began)
				{	return true;
				}
			}
		} 
		else 
		{   if( Input.GetMouseButtonDown(0) ) 
			{   return true;
			}
		}
		
		return false;
	}

	public bool isTouchUp(int index)
	{
		if( !Application.isEditor )
		{	if( Input.GetTouch(index).phase == TouchPhase.Ended ) 
			{	return true;
			}
		}
		else
		{	if( Input.GetMouseButtonUp(0) )
			{	return true;
			}
		}
		
		return false;
	}
	
	public bool isTouchUp() 
	{
		if (!Application.isEditor )  
		{   
			foreach(Touch touch in Input.touches)
			{	if(touch.phase == TouchPhase.Ended)
				{	return true;
				}
			}
		} 
		else 
		{   if( Input.GetMouseButtonUp(0) ) 
			{   return true;
			}
		}
		
		return false;
	}
	
	#region GET TOUCH POSITION IN WORLD POINT
	public Vector3 getTouchPos() 
	{
		return getTouchPos(0);
	}
	
	public Vector3 getTouchPos(int index) 
	{
		Vector2 touchInScreenPoint = getTouchScreenPos( index );
		
		return Camera.main.ScreenToWorldPoint ( new Vector3(touchInScreenPoint.x, touchInScreenPoint.y, 0) );
	}
	
	public Vector3 getTouchDownPos() 
	{
		return getTouchDownPos(0);
	}
	
	public Vector3 getTouchDownPos(int index)
	{
		Vector2 touchInScreenPoint = getTouchDownScreenPos(index);

		return Camera.main.ScreenToWorldPoint ( new Vector3( touchInScreenPoint.x, touchInScreenPoint.y, 0) );
	}
	
	public Vector3 getTouchUpPos() 
	{
		return getTouchUpPos(0);
	}
	
	public Vector3 getTouchUpPos(int index)
	{
		Vector2 touchInScreenPoint = getTouchUpScreenPos(index);
		return Camera.main.ScreenToWorldPoint ( new Vector3( touchInScreenPoint.x, touchInScreenPoint.y, 0) );
	}
	#endregion

	#region GET TOUCH POSITION IN SCREEN POINT
	public Vector2 getTouchScreenPos() 
	{
		return getTouchScreenPos(0);
	}

	public Vector2 getTouchScreenPos(int index) 
	{
		if (!Application.isEditor )  
		{   if (Input.touchCount > 0 )
			{   return Input.GetTouch(index).position;
			}
		} 
		else 
		{   if( Input.GetMouseButton(0) || Input.GetMouseButtonUp(0) || Input.GetMouseButtonDown(0)) 
			{   return Input.mousePosition;
			}
		}
		
		return Vector2.zero;
	}
	
	public Vector2 getTouchDownScreenPos()  
	{
		return getTouchDownScreenPos(0);
	}
	
	public Vector2 getTouchDownScreenPos(int index) 
	{
		if (!Application.isEditor) 
		{   if (Input.touchCount > 0 && Input.GetTouch(index).phase == TouchPhase.Began ) 
			{
				return Input.GetTouch(index).position;
			}
			
		} 
		else 
		{   if( Input.GetMouseButtonDown(0) ) 
			{
				return Input.mousePosition;
			}
		}
		
		return Vector2.zero;
	}

	public Vector2 getTouchUpScreenPos()  
	{
		return getTouchUpScreenPos(0);
	}
	
	public Vector2 getTouchUpScreenPos(int index) 
	{
		if (!Application.isEditor) 
		{   if (Input.touchCount > 0 && Input.GetTouch(index).phase == TouchPhase.Ended ) 
			{
				return Input.GetTouch(index).position;
			}
			
		} 
		else 
		{   if( Input.GetMouseButtonUp(0)) 
			{
				return Input.mousePosition;
			}
		}
		
		return Vector2.zero;
	}
	#endregion	

	public bool isSpriteTouch(GameObject sprite) 
	{	
		// inactive sprites do not receive touch
		if(!sprite.activeInHierarchy)
		{	return false;
		}
		
		else
		{	if(!Application.isEditor)
			{	for(int i=0; i< getTouchCount(); i++)
				{	if(isSpriteHitAtPos(getTouchPos(i), sprite))
					{	return true;
					}
				}
			}	
			// if editor, check for mouse position
			else
			{	if(isSpriteHitAtPos(getTouchPos(), sprite))
				{	return true;
				}
			}
		}
		
		return false;
	}
	
	bool isSpriteHitAtPos(Vector3 pos, GameObject sprite) 
	{	
		RaycastHit2D[] hits = Physics2D.RaycastAll(pos, Vector2.zero);
		
		foreach(RaycastHit2D hit in hits)
		{	if (hit && hit.collider != null) 
			{	if(hit.collider.gameObject == sprite)
				{	return true;
				}
			}
		}
		
		return false;
	}
}
