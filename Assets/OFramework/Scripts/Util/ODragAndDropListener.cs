﻿/*
	ODragAndDropListener by rsui. (09/02/14)
 
	usage: 
	Implement this abstract class in order to be able to attach
 	to ODragAndDrop script.
 */


using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public abstract class ODragAndDropListener : OStateMachineTransitionListener <ODragAndDrop.State>
{
	protected override IDictionary<ODragAndDrop.State, Action> Initialize() 
	{
		return  new Dictionary<ODragAndDrop.State, Action>
		{
			{ODragAndDrop.State.IDLE, OnIdle},
			{ODragAndDrop.State.DRAGSTARTING, OnDragStarting}, 
			{ODragAndDrop.State.DRAGSTART, OnDragStart},
			{ODragAndDrop.State.DRAGMOVING, OnDragMoving},
			{ODragAndDrop.State.DRAGIDLE, OnDragIdle},
			{ODragAndDrop.State.DRAGRELEASED, OnDragReleased}
		};
	}
	public virtual void OnIdle (){}
	public virtual void OnDragStarting() {}
	public virtual void OnDragStart (){}
	public virtual void OnDragIdle (){}
	public virtual void OnDragMoving(){}
	public virtual void OnDragReleased(){}
}