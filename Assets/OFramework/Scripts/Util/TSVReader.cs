﻿using UnityEngine;
using System.Collections;

public class TSVReader : MonoBehaviour 
{
	public static Hashtable ParseToDict(string text)
	{	
		Hashtable dict = new Hashtable();

		string[] keyValuePairs = text.Split('\n');

		for(int i=0; i<keyValuePairs.Length; i++)
		{	
			string[] keyValuePair = (keyValuePairs[i]).Split('\t');

			if(keyValuePair.Length == 2)
			{	
				if(dict.ContainsKey(keyValuePair[0]))
				{	dict[keyValuePair[0]] = keyValuePair[1];
				}	else
				{	dict.Add(keyValuePair[0], keyValuePair[1]);
				}
			}	else	
			{	Debug.LogError("TSVReader :: " + (keyValuePairs[i]) + " is invalid.");
			}
		}

		return dict;
	}

	public static Hashtable ParseToDictWithType(string text)
	{	
		Hashtable dict = new Hashtable();
		
		string[] keyValuePairs = text.Split('\n');
		
		for(int i=0; i<keyValuePairs.Length; i++)
		{	
			string[] keyValuePair = (keyValuePairs[i]).Split('\t');

			string type = keyValuePair[2];

			if(type == "bool")
			{	dict.Add(keyValuePair[0], bool.Parse(keyValuePair[1]));
			}	else if(type == "int")
			{	dict.Add(keyValuePair[0], int.Parse(keyValuePair[1]));
			}	else if(type == "float")
			{	dict.Add(keyValuePair[0], float.Parse(keyValuePair[1]));
			}	else
			{	dict.Add(keyValuePair[0], keyValuePair[1]);
			}	 
		}
		
		return dict;
	}

	public static string ConvertToTSV(Hashtable dict)
	{	
		string tsvString = "";
		
		foreach(string key in dict.Keys)
		{	tsvString += key + "\\t" + dict[key] + "\\n";	
		}

		return tsvString;
	} 	

	public static void DebugDict(Hashtable dict)
	{
		foreach(DictionaryEntry item in dict)
		{	Debug.Log(item.Key + ":" + item.Value);
		}
	}
}
