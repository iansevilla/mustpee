/*
	OStateMachineTransitionListener by rsui. (09/02/14)
 
	usage: 
	Used to call transition function defined in its child class
	See ODragAndDropListener for sample implementation
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public abstract class OStateMachineTransitionListener<T>: MonoBehaviour where T: struct
{
	protected IDictionary<T, Action> m_functionDictionary;
	protected abstract IDictionary<T, Action> Initialize ();

	public virtual void Start()
	{
		m_functionDictionary = Initialize () ?? new Dictionary<T, Action>();
	}
	
	public void CallFor(T state)
	{
		if (m_functionDictionary == null)
				return;

		if (m_functionDictionary.ContainsKey (state))
			m_functionDictionary[state]();		
	}
}

