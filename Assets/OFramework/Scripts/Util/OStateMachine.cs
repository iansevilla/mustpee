/*
	OStateMachine by rsui. (09/02/14)
 
	usage: 
	Simple state machine. Override InitializeListeners to 
	get the listeners attached and be put under _listeners.

	Call change state in order to change to target state and 
	call the listeners if there are any attached
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public abstract class OStateMachine<T>: MonoBehaviour where T: struct
{
	protected OStateMachineTransitionListener<T>[] _listeners;
	public T m_currentState;

	public virtual void Awake()
	{
		_listeners = InitializeListeners () ?? 
			new OStateMachineTransitionListener<T>[0] ;
	}

	protected virtual OStateMachineTransitionListener<T>[] InitializeListeners ()
	{
		return null;
	}

	public void ChangeState(T toState)
	{
		m_currentState = toState;
		foreach (var listener in _listeners) 
			listener.CallFor (toState);	
	}
}
