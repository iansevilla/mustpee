﻿using UnityEngine;
using System.Collections;
using System.IO;

public class OUtil : MonoBehaviour 
{
	public static string getByteForTexture(Texture2D photo)
	{
		if(photo.format == TextureFormat.ARGB32 || photo.format == TextureFormat.RGBA32 || 
		   		photo.format == TextureFormat.RGB24 || photo.format == TextureFormat.Alpha8)
		{	
			byte[] screenshot = photo.EncodeToPNG();
			string screenshotStr = System.Text.Encoding.UTF8.GetString(screenshot);
		
			Debug.Log("OUtil :: getByteForTexture " + screenshotStr);
			
			return screenshotStr;
		}	
		
		else
		{	Debug.Log("OUtil :: Texture format needs to be ARGB32, RGBA32, RGB24 or Alpha8");
		}

		return "";
	}

	public static Texture2D getCameraTexture(Camera cam) 
	{
		Debug.Log("OUtil :: getCameraTexture");

		cam.targetTexture = new RenderTexture(1024, 1024, 24, RenderTextureFormat.ARGB32);
		RenderTexture.active = cam.targetTexture;
		cam.Render();
		Texture2D image = new Texture2D(cam.targetTexture.width, cam.targetTexture.height);
		image.ReadPixels(new Rect(0, 0, cam.targetTexture.width, cam.targetTexture.height), 0, 0);
		image.Apply();
		
		RenderTexture.active = null;
		cam.targetTexture = null;
		return image;
	}
	
	public static string getBytesForCameraTexture(Camera cam)
	{	
		Debug.Log("OUtil :: getBytesForCameraTexture");
		return getByteForTexture(getCameraTexture(cam));	
	}

	public static void saveTexture(Texture2D texture, string fileName)
	{
		Debug.Log("OUtil :: saveTexture");

		byte[] textureBuffer = texture.EncodeToPNG();
		
		string filePath = Application.persistentDataPath + "/" +fileName;
		File.WriteAllBytes(filePath, textureBuffer);
	}

	public static bool IsInternetAvailable()
	{
		//excerpt from code
		bool isConnectedToInternet = false;
		
		#if UNITY_EDITOR
		if (Network.player.ipAddress.ToString() != "127.0.0.1")
		{
			isConnectedToInternet = true;      
		}
		#endif
		
		#if UNITY_IPHONE
		if (iPhoneSettings.internetReachability == iPhoneNetworkReachability.ReachableViaWiFiNetwork)
		{
			isConnectedToInternet = true;
		}
		#endif
		
		#if UNITY_ANDROID
		if (iPhoneSettings.internetReachability == iPhoneNetworkReachability.ReachableViaWiFiNetwork)
		{
			isConnectedToInternet = true;
		}
		#endif
		
		return isConnectedToInternet;
	}
}
