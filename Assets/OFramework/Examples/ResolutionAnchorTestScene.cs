﻿using UnityEngine;
using System.Collections;

public class ResolutionAnchorTestScene : TestScene
{
	public TextMesh m_topAnchorLabel;
	public TextMesh m_bottomAnchorLabel;
	public TextMesh m_leftAnchorLabel;
	public TextMesh m_rightAnchorLabel;
	public TextMesh m_centerAnchorLabel;	

	public override void Start() 
	{	base.Start();

		if(m_topAnchorLabel != null)
		{	m_topAnchorLabel.text = "Top\nScreenPt:" + OResolutionHandler.screenHeight() + " \nWorldPt:" + OResolutionHandler.T().ToString("F2");
			((OAnchor)m_topAnchorLabel.GetComponent<OAnchor>()).setAnchor();
		}	else
		{	Debug.LogWarning("ResolutionTestScene :: Assign TopAnchorLabel!");
		}

		if(m_bottomAnchorLabel != null)
		{	m_bottomAnchorLabel.text = "Bottom\nScreenPt:0\nWorldPt:" + OResolutionHandler.B().ToString("F2");
			((OAnchor)m_bottomAnchorLabel.GetComponent<OAnchor>()).setAnchor();
		}	else
		{	Debug.LogWarning("ResolutionTestScene :: Assign BottomAnchorLabel!");
		}

		if(m_leftAnchorLabel != null)
		{	m_leftAnchorLabel.text = "Left\nScreenPt:0\nWorldPt:" + OResolutionHandler.L().ToString("F2");
			((OAnchor)m_leftAnchorLabel.GetComponent<OAnchor>()).setAnchor();
		}	else
		{	Debug.LogWarning("ResolutionTestScene :: Assign LeftAnchorLabel!");
		}

		if(m_rightAnchorLabel != null)
		{	m_rightAnchorLabel.text = "Right\nScreenPt:" + OResolutionHandler.screenWidth() + "\nWorldPt:" + OResolutionHandler.R().ToString("F2");
			((OAnchor)m_rightAnchorLabel.GetComponent<OAnchor>()).setAnchor();
		}	else
		{	Debug.LogWarning("ResolutionTestScene :: Assign RightAnchorLabel!");
		}

		if(m_centerAnchorLabel != null)
		{	m_centerAnchorLabel.text = "Center\nScreenPt:" + (OResolutionHandler.screenWidth()/2).ToString("F2") + ", " + (OResolutionHandler.screenHeight()/2).ToString("F2") + 
			"\nWorldPt:" + (OResolutionHandler.C().x).ToString("F2") + ", " + (OResolutionHandler.C().y).ToString("F2");
			((OAnchor)m_centerAnchorLabel.GetComponent<OAnchor>()).setAnchor();
		}	else
		{	Debug.LogWarning("ResolutionTestScene :: Assign RightAnchorLabel!");
		}
	}
}
