using UnityEngine;
using System.Collections;

public class ObjMgrTestScene : OGameScene {

	// Use this for initialization
	void Start () {
		OObjectMgr.Instance.AddNewObject ("Cube", 10, "ObjMgrTestScene");
		Debug.Log ("Create Object");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public override void OnButtonUp (OButton button)
	{
		base.OnButtonUp (button);
		
		if (button.m_buttonId == "create") {
			OObjectMgr.Instance.Spawn2D("Cube", Vector2.zero);
		}
		if (button.m_buttonId == "destroyall") {
			OObjectMgr.Instance.DestroyAll();
		}
		if (button.m_buttonId == "resetall") {
			OObjectMgr.Instance.ResetAll();
		}
		if (button.m_buttonId == "camerashake") {
			OCameraMgr.Instance.CameraShake(0.5f,0.5f);
		}
		if (button.m_buttonId == "LoadLevelButton") {
			Application.LoadLevel(0);
			Debug.Log ("Load Level");
		}



	}
}
