﻿using UnityEngine;
using System.Collections;

public class ButtonTestScene : OGameScene {

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public override void OnButtonUp (OButton button)
	{
		if (button.name == "pause") {
			OGameMgr.Instance.SetPaused(true);
		}
	}
	
	public override void OnScenePause (bool paused)
	{

	}
}
