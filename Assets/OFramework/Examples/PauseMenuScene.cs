﻿using UnityEngine;
using System.Collections;

public class PauseMenuScene : OPopupScene {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public override void OnButtonDown (OButton button)
	{
		base.OnButtonDown (button);


		Debug.Log (button);
		if (button.m_buttonId == "btn_resume")
		{
			OGameMgr.Instance.HidePopup();
			OGameMgr.Instance.SetPaused(false);
		}
	}
}
