﻿using UnityEngine;
using System.Collections;

public class DimmerFaderTestScene : OGameScene {

	// Use this for initialization
	void Start () {
		OGameMgr.Instance.Fader.Fade(Color.red, 0, 1, 2f, deactivateOnFadeEnd: false, onFadeEnd: OnFadeEnd);
	}
	
	void OnFadeEnd() {
		Debug.Log("fade end");
		OGameMgr.Instance.Fader.Fade(Color.red, 1, 0, 1f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
