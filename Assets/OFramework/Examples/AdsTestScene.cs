﻿using UnityEngine;
using System.Collections;

public class AdsTestScene : TestScene
{
	public OTextButton m_showInMobiButton;
	public OTextButton m_showInMobiBottomButton;
	public OTextButton m_removeInMobiButton;
	
	public override void Start() 
	{	base.Start();
		
		if(m_showInMobiButton == null)
		{	Debug.LogWarning("AdsTestScene :: Assign ShowInMobiButton!");
		}

		if(m_showInMobiBottomButton == null)
		{	Debug.LogWarning("AdsTestScene :: Assign ShowInMobiBottomButton!");
		}
		
		if(m_removeInMobiButton == null)
		{	Debug.LogWarning("AdsTestScene :: Assign RemoveInMobiButton!");
		}
	}
	
	public override void OnButtonUp(OButton button) 
	{	
		if(button == m_backButton)
		{	OInMobiHandler.Instance.removeBannerAd();
		}	
		else if(button == m_showInMobiButton)
		{	OInMobiHandler.Instance.createBannerAd();
		}	
		else if(button == m_showInMobiBottomButton)
		{	OInMobiHandler.Instance.createBottomBannerAd();
		}
		else if(button == m_removeInMobiButton)
		{	OInMobiHandler.Instance.removeBannerAd();
		}
	}
}
