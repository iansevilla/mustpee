﻿using UnityEngine;
using System.Collections;

public class PauseTestScene : OGameScene {

	// Use this for initialization
	void Start () {
		LeanTween.scale(gameObject, new Vector3(1.1f, 1.1f, 1f), 0.5f).setLoopPingPong().setEase(LeanTweenType.easeOutExpo);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public override void OnButtonUp (OButton button)
	{
		base.OnButtonUp (button);
		
		Debug.Log (button);
		if (button.m_buttonId == "pause") {
			OGameMgr.Instance.SetPaused(true);
		}
	}
	
	public override void OnScenePause (bool paused)
	{
		if (paused)
		{
			Debug.Log("Showing Pause Menu...");
			OGameMgr.Instance.ShowPopup("PauseMenuScene");
		} else {
		}
	}
}
