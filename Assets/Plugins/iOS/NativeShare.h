#import <Foundation/Foundation.h>
#import <Social/Social.h>

// Root view controller of Unity screen
extern UIViewController *UnityGetGLViewController();

@interface NativeShare : NSObject

-(bool) isFacebookAvailable;
-(void) facebookShare: (const char*) msg;
-(void) facebookShare: (const char*) msg withPhoto: (const char*) photoBytes;
-(void) facebookShare: (const char*) msg withLink: (const char*) link;
-(void) facebookShare: (const char*) msg withPhoto: (const char*) photoBytes withLink: (const char*) link;
-(bool) isTwitterAvailable;
-(void) tweet: (const char*) msg;
-(void) tweet: (const char*) msg withPhoto: (const char*) photoFilename;
-(void) tweet: (const char*) msg withLink: (const char*) link;
-(void) tweet: (const char*) msg withPhoto: (const char*) photoFilename withLink: (const char*) link;

@end
