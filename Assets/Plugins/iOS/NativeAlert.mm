#import "NativeAlert.h"

@implementation NativeAlert

- (id)init
{
    self = [super init];
    
    return self;
}

- (void)createAlert: (const char*) title withMsg: (const char*) msg withCancelButtonTitle: (const char*) cancelButtonTitle withOtherButtonTitle: (const char*) otherButtonTitle
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: [NSString stringWithUTF8String: title] message: [NSString stringWithUTF8String: msg] delegate: self cancelButtonTitle: [NSString stringWithUTF8String: cancelButtonTitle] otherButtonTitles: [NSString stringWithUTF8String: otherButtonTitle], nil];
    [alert show];
}

- (void)createAlert: (const char*) title withMsg: (const char*) msg withCancelButtonTitle: (const char*) cancelButtonTitle
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: [NSString stringWithUTF8String: title] message: [NSString stringWithUTF8String: msg] delegate: self cancelButtonTitle: [NSString stringWithUTF8String: cancelButtonTitle] otherButtonTitles: nil];
    [alert show];
}


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
     UnitySendMessage("~OAlertHandler", "didDismissWithButtonIndex", [[NSString stringWithFormat: @"%d", buttonIndex] cStringUsingEncoding:NSASCIIStringEncoding]);
}

@end

static NativeAlert *nativeAlertPlugin = nil;

extern "C"
{
	void _CreateAlert(const char* title, const char* msg, const char* cancelButtonTitle, const char* otherButtonTitle)
	{
        NSLog(@"NativeAlert :: CreateAlert %@", [NSString stringWithUTF8String: title]);
        
        if (nativeAlertPlugin == nil)
			nativeAlertPlugin = [[NativeAlert alloc] init];
        
        [nativeAlertPlugin createAlert: title withMsg: msg withCancelButtonTitle: cancelButtonTitle withOtherButtonTitle: otherButtonTitle];
    }
    
    void _CreateAlertOK(const char* title, const char* msg, const char* cancelButtonTitle)
	{
        NSLog(@"NativeAlert :: CreateAlert %@", [NSString stringWithUTF8String: title]);
        
        if (nativeAlertPlugin == nil)
			nativeAlertPlugin = [[NativeAlert alloc] init];
        
        [nativeAlertPlugin createAlert: title withMsg: msg withCancelButtonTitle: cancelButtonTitle];
    }

}