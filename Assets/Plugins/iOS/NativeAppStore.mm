#import "NativeAppStore.h"

@implementation NativeAppStore

- (id)init
{
    self = [super init];
    
    return self;
}

-(void) openAppInStore: (int) appID
{
    NSDictionary *parameters = @{SKStoreProductParameterITunesItemIdentifier: [NSNumber numberWithInteger: appID]};
    
    SKStoreProductViewController *productViewController = [[SKStoreProductViewController alloc] init];
    [productViewController loadProductWithParameters:parameters completionBlock:nil];
    [productViewController setDelegate:self];
    [UnityGetGLViewController() presentViewController:productViewController animated:YES completion:nil];
}

-(void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController
{   [viewController dismissViewControllerAnimated:YES completion:nil];
    
    
    UnitySendMessage("~OAppStoreHandler", "appStoreClosed", "");
}

- (BOOL)prefersStatusBarHidden
{   return YES;
}

@end

static NativeAppStore *nativeAppStorePlugin = nil;

extern "C"
{
	void _OpenAppInStore(int appID)
	{
        NSLog(@"NativeAppStore :: Open App %d", appID);
        
        if (nativeAppStorePlugin == nil)
			nativeAppStorePlugin = [[NativeAppStore alloc] init];
        
        [nativeAppStorePlugin openAppInStore: appID];
    }
}