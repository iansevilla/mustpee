﻿using UnityEngine;
using System.Collections;

public class SSplashPopup: OPopupScene
{
	public GameObject boy;
	public GameObject hand;
	public GameObject boyBlack;
	public GameObject handBlack;
	public GameObject blink;
	
	public GameObject boySmoke1;
	public GameObject boySmoke2;
	public GameObject boySmoke3;
	public GameObject handSmoke;
	
	public GameObject text;
	public GameObject button;
	public GameObject buttonDown;
	public GameObject buttonBlack;
	
	public GameObject buttonPieces;
	public GameObject buttonExplosion; 
	
	int handButtonDownCount = 0;
	
	public delegate void Action();
	public static Action OnFinishSplash = delegate {};
	
	public AudioClip m_clickSound;
	public AudioClip m_explodeSound;	
	
	void Start () 
	{	
		m_isAcceptTouch = false;
		
		if(boy !=null)
		{	boy.SetActive(true);
			boy.transform.position = new Vector3(0, 1, 0);
		}	else
		{	Debug.Log("SplashScene :: Please set up Boy");
		}
		
		if(hand !=null)
		{	hand.SetActive(true);
			hand.transform.localScale = new Vector3(1, 0.65f, 1);
			hand.transform.position = new Vector3(2.3f, -3.8f, 0);
		}	else
		{	Debug.Log("SplashScene :: Please set up Hand");
		}
		
		if(boyBlack !=null)
		{	boyBlack.SetActive(false);
			boyBlack.transform.position = new Vector3(0, 1.88f, 0);
		}	else
		{	Debug.Log("SplashScene :: Please set up Boy Black");
		}
		
		if(handBlack !=null)
		{	handBlack.SetActive(false);
			handBlack.transform.position = new Vector3(2.3f, -3, 0);
		}	else
		{	Debug.Log("SplashScene :: Please set up Hand Black");
		}
		
		if(blink !=null)
		{	blink.SetActive(false);
			blink.transform.position = new Vector3(0.1f, 0.88f, 0);		
		}	else
		{	Debug.Log("SplashScene :: Please set up Blink");
		}
		
		if(boySmoke1 !=null)
		{	boySmoke1.SetActive(false);
			boySmoke1.transform.position = new Vector3(-2, 5, 0);
			boySmoke1.transform.localScale = new Vector3(1, 0.1f, 1);
		}	else
		{	Debug.Log("SplashScene :: Please set up Boy Smoke 1");
		}
		
		if(boySmoke2 !=null)
		{	boySmoke2.SetActive(false);
			boySmoke2.transform.position = new Vector3(0, 5, 0);
			boySmoke2.transform.localScale = new Vector3(1, 0.1f, 1);
		}	else
		{	Debug.Log("SplashScene :: Please set up Boy Smoke 2");
		}
		
		if(boySmoke3 !=null)
		{	boySmoke3.SetActive(false);
			boySmoke3.transform.position = new Vector3(2, 5, 0);
			boySmoke3.transform.localScale = new Vector3(1, 0.1f, 1);
		}	else
		{	Debug.Log("SplashScene :: Please set up Boy Smoke 3");
		}
		
		if(handSmoke !=null)
		{	handSmoke.SetActive(false);
			handSmoke.transform.position = new Vector3(2.9f, -2.5f, 0);
			handSmoke.transform.localScale = new Vector3(0.5f, 0.1f, 0.5f);
		}	else
		{	Debug.Log("SplashScene :: Please set up Hand Smoke");
		}
		
		if(text !=null)
		{	text.SetActive(true);
			text.transform.position = new Vector3(0, -5, 0);
		}	else
		{	Debug.Log("SplashScene :: Please set up Text");
		}
		
		if(button !=null)
		{	button.SetActive(true);
			button.transform.position = new Vector3(2.2f, -4.5f, 0);
		}	else
		{	Debug.Log("SplashScene :: Please set up Button");
		}
		
		if(buttonDown !=null)
		{	buttonDown.SetActive(false);
			buttonDown.transform.position = new Vector3(2.2f, -4.5f, 0);
		}	else
		{	Debug.Log("SplashScene :: Please set up Button Down");
		}
		
		if(buttonBlack !=null)
		{	buttonBlack.SetActive(false);
			buttonBlack.transform.position = new Vector3(2.2f, -4.3f, 0);
		}	else
		{	Debug.Log("SplashScene :: Please set up Button Black");
		}
		
		if(buttonPieces !=null)
		{	buttonPieces.SetActive(false);
			buttonPieces.transform.localPosition = new Vector3(2.2f, -4, 0);
			buttonPieces.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
		}	else
		{	Debug.Log("SplashScene :: Please set up Button Pieces");
		}
		
		if(buttonExplosion !=null)
		{	buttonExplosion.SetActive(false);
			buttonExplosion.transform.localPosition = new Vector3(2.2f, -3.8f, 0);
			buttonExplosion.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
		}	else
		{	Debug.Log("SplashScene :: Please set up Button Explosion");
		}
		
		/**
		// first frame
		hand.SetActive(false);
		button.SetActive(false);
		buttonDown.SetActive(true);
		**/
		
		// start anim
		handButtonAnimDown();
	}
	
	void handButtonAnimDown()
	{	
		handButtonDownCount++;
		
		if(handButtonDownCount < 10)
		{	LeanTween.scaleY(hand, 0.65f, 0.002f).setOnComplete(handButtonAnimUp);
			LeanTween.moveY(hand, -3.8f, 0.002f);
		}	else
		{	LeanTween.scaleY(hand, 0.65f, 0.002f).setOnComplete(buttonExplodeAnim1);
			LeanTween.moveY(hand, -3.8f, 0.002f);
		}
		
		button.SetActive(true);
		buttonDown.SetActive(false);		
	}
	
	void handButtonAnimUp()
	{	
		LeanTween.scaleY(hand, 1, 0.001f).setOnComplete(handButtonAnimDown);
		LeanTween.moveY(hand, -2.5f, 0.001f);
		
		button.SetActive(false);
		buttonDown.SetActive(true);
		
		OAudioMgr.Instance.PlaySFX(m_clickSound);
	}
	
	void buttonExplodeAnim1()
	{	
		OAudioMgr.Instance.PlaySFX(m_explodeSound);
		
		Invoke("boyDisappearAnim", timeForFrameCount(2));
		
		Invoke("handDisappearAnim", timeForFrameCount(1));
		
		button.SetActive(false);
		buttonDown.SetActive(true);
		Invoke("buttonExplodeAppearAnim", timeForFrameCount(3));
		
		LeanTween.moveY(text, -4.7f, timeForFrameCount(4));
		
		buttonPieces.SetActive(true);
		LeanTween.scale(buttonPieces, new Vector3(0.3f, 0.3f, 0.3f), timeForFrameCount(2)).setOnComplete(buttonExplodeAnim2);		
		LeanTween.move(buttonPieces, new Vector2(2.2f, -2.5f), timeForFrameCount(2));
		
		buttonExplosion.SetActive(true);
		LeanTween.scale(buttonExplosion, new Vector3(0.7f, 0.7f, 0.7f), timeForFrameCount(2));		
		LeanTween.move(buttonExplosion, new Vector2(1.5f, 0.7f), timeForFrameCount(2));
		
	}
	
	void handDisappearAnim()
	{	hand.SetActive(false);		
	}
	
	void boyDisappearAnim()
	{	boy.SetActive(false);		
	}
	
	void buttonExplodeAppearAnim()
	{
		button.SetActive(false);
		buttonDown.SetActive(false);
		buttonBlack.SetActive(true);
	}
	
	void buttonExplodeAnim2()
	{
		Invoke("boyHandBlackAppearAnim", timeForFrameCount(1));
		
		LeanTween.moveY(text, -5f, timeForFrameCount(6));
		
		LeanTween.scale(buttonPieces, new Vector3(0.5f, 0.5f, 0.5f), timeForFrameCount(3)).setOnComplete(smokeAnim);		
		iTween.FadeTo(buttonPieces, iTween.Hash("alpha", 0, "time", timeForFrameCount(3)));		
		
		LeanTween.scale(buttonExplosion, new Vector3(0.9f, 0.9f, 0.9f), timeForFrameCount(3));		
		iTween.FadeTo(buttonExplosion, iTween.Hash("alpha", 0, "time", timeForFrameCount(3)));	
	}
	
	void boyHandBlackAppearAnim()
	{	
		boyBlack.SetActive(true);
		handBlack.SetActive(true);
	}
	
	void smokeAnim()
	{
		Invoke("blinkAppearAnim", timeForFrameCount(16));
		Invoke("blinkAppearAnim", timeForFrameCount(23));
		
		int smokeTime = 50;
		
		boySmoke1.SetActive(true);
		LeanTween.moveY(boySmoke1, 9, timeForFrameCount(smokeTime)).setOnComplete(finishAnim);	
		LeanTween.scaleY(boySmoke1, 1, timeForFrameCount(smokeTime));
		iTween.FadeTo(boySmoke1, iTween.Hash("alpha", 0, "time", timeForFrameCount(smokeTime)));	
		
		boySmoke2.SetActive(true);
		LeanTween.moveY(boySmoke2, 10, timeForFrameCount(smokeTime));
		LeanTween.scaleY(boySmoke2, 1, timeForFrameCount(smokeTime));
		iTween.FadeTo(boySmoke2, iTween.Hash("alpha", 0, "time", timeForFrameCount(smokeTime)));	
		
		boySmoke3.SetActive(true);
		LeanTween.moveY(boySmoke3, 9, timeForFrameCount(smokeTime));		
		LeanTween.scaleY(boySmoke3, 1, timeForFrameCount(smokeTime));
		iTween.FadeTo(boySmoke3, iTween.Hash("alpha", 0, "time", timeForFrameCount(smokeTime)));	
		
		handSmoke.SetActive(true);
		LeanTween.move(handSmoke, new Vector2(3.2f, -0.6f), timeForFrameCount(smokeTime));	
		LeanTween.scaleY(handSmoke, 0.5f, timeForFrameCount(smokeTime));
		iTween.FadeTo(handSmoke, iTween.Hash("alpha", 0, "time", timeForFrameCount(smokeTime)));		
	}
	
	void blinkAppearAnim()
	{	blink.SetActive(true);
		
		Invoke("blinkDisappearAnim",  timeForFrameCount(3));
	}
	
	void blinkDisappearAnim()
	{	blink.SetActive(false);
	}
	
	void finishAnim()
	{	OnFinishSplash();
		OGameMgr.Instance.ShowPopup("SHomePopup");
	}
	
	float timeForFrameCount(float frameCount)
	{	float fps = 24;
		//float fps = 15;
		return frameCount/fps;
	}
	
	
	
}
