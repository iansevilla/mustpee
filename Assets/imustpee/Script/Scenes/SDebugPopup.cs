﻿using UnityEngine;
using System.Collections;

public class SDebugPopup : OPopupScene {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void OnButtonUp (OButton button)
	{
		if (button.m_buttonId == "btn_back")
		{	OGameMgr.Instance.ShowPopup("SHomePopup");
		}

		else if (button.m_buttonId == "btn_banner")
		{	OInMobiHandler.Instance.createBannerAd();
		}

		else if (button.m_buttonId == "btn_interstitial")
		{	
		}

		else if (button.m_buttonId == "btn_sendevent")
		{	OGAHandler.Instance.SendEvent(OGAKeys.CATEGORY_KPI, OGAKeys.ACTION_APP_FLOW_INTERRUPT, OGAKeys.LABEL_TOTAL_PLAY_TIME, 1.0f);
		}
	}
}
