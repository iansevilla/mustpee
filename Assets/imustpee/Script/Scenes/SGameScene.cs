﻿using UnityEngine;
using System.Collections;

public class SGameScene : OGameScene {
	
	// Use this for initialization
	void Start () {
		OGameMgr.Instance.ShowPopup("SSplashPopup");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void OnButtonDown(OButton button)
	{
		if (button.m_buttonId == "btn_maledoor")
		{	LevelManager.instance.OpenMaleDoor(true);
		}
		
		else if (button.m_buttonId == "btn_femaledoor")
		{	LevelManager.instance.OpenFemaleDoor(true);
		}
	}

	public override void OnButtonUp (OButton button)
	{
		if (button.m_buttonId == "btn_pause")
		{	OGameMgr.Instance.SetPaused(true);
			OGameMgr.Instance.ShowPopup("SPausePopup");
		}

		else if (button.m_buttonId == "btn_mainScreen")
		{	OGameMgr.Instance.ShowPopup("SHomePopup");
		}

		else if (button.m_buttonId == "btn_resultScreen")
		{	OGameMgr.Instance.ShowPopup("SResultPopup");
		}


		if (button.m_buttonId == "btn_maledoor")
		{	LevelManager.instance.OpenMaleDoor(false);
		}
		
		else if (button.m_buttonId == "btn_femaledoor")
		{	LevelManager.instance.OpenFemaleDoor(false);
		}

	}


}
