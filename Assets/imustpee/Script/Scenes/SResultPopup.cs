using UnityEngine;
using System.Collections;

public class SResultPopup : OPopupScene {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void OnButtonUp (OButton button)
	{
		if (button.m_buttonId == "btn_mainScreen")
		{	OGameMgr.Instance.ShowPopup("SHomePopup");
		}

		else if (button.m_buttonId == "btn_InGame")
		{	OGameMgr.Instance.HidePopup();
		}
	}
}
