﻿using UnityEngine;
using System.Collections;

public class GirlScript : MonoBehaviour {
	
	//Boy Variables
	private int 	m_GirlDirection;
	private float 	m_GirlSpeed;

	//Audio
	public AudioClip m_sfx_success;

	//Sprites
	public SpriteRenderer m_GirlSprite;
	
	void Start () {
		
		m_GirlDirection 	= GirlConfig.GIRL_STARTING_DIRECTION;
		m_GirlSpeed 		= GirlConfig.GIRL_SPEED;
		
	}
	
	void Update () {
		if(m_GirlDirection == -1)
		{
			m_GirlSprite.transform.rotation = Quaternion.Euler (0, 180, 0);
			transform.Translate(Vector3.left * Time.deltaTime * m_GirlSpeed);
		}
		if(m_GirlDirection == 1)
		{
			m_GirlSprite.transform.rotation = Quaternion.Euler (0, 0, 0);
			transform.Translate(Vector3.right * Time.deltaTime * m_GirlSpeed);
		}
	}

	void LateUpdate()
	{
		if(!LevelManager.instance.Get_IsGameStart())
			Destroy(this.gameObject);
	}

	void OnTriggerEnter2D(Collider2D other) 
	{
		if(other.tag == "DoorFemale")
		{
			OAudioMgr.Instance.PlaySFX(m_sfx_success);
			PlayerScript.instance.Set_PlayerScore(GirlConfig.GIRL_SUCCESS_POINTS);
			Destroy(this.gameObject);
		}
		if(other.tag == "DoorMale")
		{
			WrongDoor();
		}

		if(other.tag == "DestryoObject")
		{
			YouMissedThisPerson();
		}
	}

	void WrongDoor()
	{
		OCameraMgr.Instance.CameraShake(.3f, .3f);
		FadeAllPeople ();
		
		this.gameObject.renderer.material.color = new Color(1f,1f,1f, 1f);
		PlayerScript.instance.Set_PlayerIsAlive(false);
	}
	
	void YouMissedThisPerson()
	{
		OCameraMgr.Instance.CameraShake(.3f, .3f);
		FadeAllPeople ();
		
		LeanTween.moveLocalX (this.gameObject, 7f, .5f).setEase (LeanTweenType.easeInOutCirc);
		this.gameObject.renderer.material.color = new Color(1f,1f,1f, 1f);
		PlayerScript.instance.Set_PlayerIsAlive(false);
	}
	
	void FadeAllPeople()
	{
		foreach(GameObject goMale in GameObject.FindGameObjectsWithTag("PeeMale"))
		{
			goMale.gameObject.GetComponent<BoyScript>().Set_BoySpeed(0f);
			goMale.renderer.material.color = new Color(1f,1f,1f, 0.3f);
			goMale.gameObject.collider2D.enabled = false;
		}
		
		foreach(GameObject goFemale in GameObject.FindGameObjectsWithTag("PeeFemale"))
		{
			goFemale.gameObject.GetComponent<GirlScript>().Set_GirlSpeed(0f);
			goFemale.renderer.material.color = new Color(1f,1f,1f, 0.3f);
			goFemale.gameObject.collider2D.enabled = false;
		}
	}

	public void Set_GirlSpeed(float fGirlSpeed)
	{	m_GirlSpeed = fGirlSpeed;
	}

	public void Set_GirlDirection(int nDirection)
	{	m_GirlDirection = nDirection;
	}
	
	public int Get_BoyDirection()
	{	return m_GirlDirection;
	}
}
