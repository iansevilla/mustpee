﻿using UnityEngine;
using System.Collections;

public class BoyScript : MonoBehaviour {

	//Boy Variables
	private int 	m_BoyDirection;
	private float 	m_BoySpeed;

	//Audio
	public AudioClip m_sfx_success;

	//Sprites
	public SpriteRenderer m_BoySprite;

	void Start () {

		m_BoyDirection 	= BoyConfig.BOY_STARTING_DIRECTION;
		m_BoySpeed 		= BoyConfig.BOY_SPEED;

		Debug.Log ("Get CURRENT ROUND " + LevelManager.instance.Get_LevelRound ());

	}

	void Update () {
		if(m_BoyDirection == -1)
		{
			m_BoySprite.transform.rotation = Quaternion.Euler (0, 180, 0);
			transform.Translate(Vector3.left * Time.deltaTime * m_BoySpeed);
		}
		if(m_BoyDirection == 1)
		{
			m_BoySprite.transform.rotation = Quaternion.Euler (0, 0, 0);
			transform.Translate(Vector3.right * Time.deltaTime * m_BoySpeed);
		}
	}

	void LateUpdate()
	{
		if(!LevelManager.instance.Get_IsGameStart())
			Destroy(this.gameObject);
	}

	void OnTriggerEnter2D(Collider2D other) 
	{
		if(other.tag == "DoorMale")
		{
			OAudioMgr.Instance.PlaySFX(m_sfx_success);
			PlayerScript.instance.Set_PlayerScore(BoyConfig.BOY_SUCCESS_POINTS);
			Destroy(this.gameObject);
		}
		if(other.tag == "DoorFemale")
		{
			WrongDoor();
		}

		if(other.tag == "DestryoObject")
		{
			YouMissedThisPerson();
		}
	}

	void WrongDoor()
	{
		OCameraMgr.Instance.CameraShake(.3f, .3f);
		FadeAllPeople ();

		this.gameObject.renderer.material.color = new Color(1f,1f,1f, 1f);
		PlayerScript.instance.Set_PlayerIsAlive(false);
	}

	void YouMissedThisPerson()
	{
		OCameraMgr.Instance.CameraShake(.3f, .3f);
		FadeAllPeople ();

		LeanTween.moveLocalX (this.gameObject, 7f, .5f).setEase (LeanTweenType.easeInOutCirc);
		this.gameObject.renderer.material.color = new Color(1f,1f,1f, 1f);
		PlayerScript.instance.Set_PlayerIsAlive(false);
	}

	void FadeAllPeople()
	{
		foreach(GameObject goMale in GameObject.FindGameObjectsWithTag("PeeMale"))
		{
			goMale.gameObject.GetComponent<BoyScript>().Set_BoySpeed(0f);
			goMale.renderer.material.color = new Color(1f,1f,1f, 0.3f);
			goMale.gameObject.collider2D.enabled = false;
		}
		
		foreach(GameObject goFemale in GameObject.FindGameObjectsWithTag("PeeFemale"))
		{
			goFemale.gameObject.GetComponent<GirlScript>().Set_GirlSpeed(0f);
			goFemale.renderer.material.color = new Color(1f,1f,1f, 0.3f);
			goFemale.gameObject.collider2D.enabled = false;
		}
	}

	public void Set_BoySpeed(float fBoySpeed)
	{	m_BoySpeed = fBoySpeed;
	}

	public void Set_BoyDirection(int nDirection)
	{	m_BoyDirection = nDirection;
	}

	public int Get_BoyDirection()
	{	return m_BoyDirection;
	}
}
