﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {

	public static PlayerScript instance;

	//Label
	public OLabel m_lblPlayerScore;

	//Player Variables
	private bool m_bPlayerIsAlive = false;
	private int	 m_nPlayerScore = 0;
	

	void Start () {
		instance = this;
	}

	void Update () {
	}

	public bool Get_PlayerIsAlive()
	{	return m_bPlayerIsAlive;
	}

	public void Set_PlayerIsAlive(bool bIsAlive)
	{	m_bPlayerIsAlive = bIsAlive;
	}

	public int Get_PlayerScore()
	{	return m_nPlayerScore;
	}

	public void Set_PlayerScore(int nGainScore)
	{	m_nPlayerScore += nGainScore;
		m_lblPlayerScore.UpdateLabel (m_nPlayerScore);
	}

	public void Reset_PlayerScore()
	{	m_nPlayerScore = 0;
		m_lblPlayerScore.UpdateLabel (m_nPlayerScore);
	}

	public void ResetPlayer()
	{	Set_PlayerIsAlive (true);
		Reset_PlayerScore();
	}


}
