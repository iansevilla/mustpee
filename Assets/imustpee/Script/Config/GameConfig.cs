﻿public class GameConfig
{
	public const float		DELAY_TO_SHOW_RESULT_SCREEN = 1f;
}

public class PlayerConfig
{
}


public class BoyConfig
{
	public const float		BOY_SPEED 					= 12f;
	public const int		BOY_STARTING_DIRECTION 		= 1;
	public const int		BOY_SUCCESS_POINTS			= 1;
	public const int		BOY_MISSED_POINTS			= -2;
}

public class GirlConfig
{
	public const float		GIRL_SPEED 					= 12f;
	public const int		GIRL_STARTING_DIRECTION 	= 1;
	public const int		GIRL_SUCCESS_POINTS			= 1;
	public const int		GIRL_MISSED_POINTS			= -2;
}

public class DoorConfig
{
}