﻿using UnityEngine;
using System.Collections;

public class SampleTween : MonoBehaviour {

	public GameObject sampleTween;

	// Use this for initialization
	void Start () {
	
		MoveObject1 ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void MoveObject1()
	{
		LeanTween.moveX (sampleTween, sampleTween.transform.position.x + 8, 1).setEase(LeanTweenType.easeSpring).setOnComplete(MoveObject2);
	}
	void MoveObject2()
	{
		LeanTween.moveX(sampleTween, sampleTween.transform.position.x - 8, 1).setEase(LeanTweenType.easeOutBounce).setOnComplete(MoveObject1);
	}
}
