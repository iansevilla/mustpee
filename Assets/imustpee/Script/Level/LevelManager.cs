﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

	public static LevelManager instance;

	private int 		m_nPlayerRound = 0;
	private bool		m_bIsGameStart = false;
	private bool		m_bTempCallOnce = false;

	//GameObjects
	public GameObject 	m_goBoy;
	public GameObject 	m_goGirl;

	public GameObject	m_goDoorMale;
	public GameObject	m_goDoorFemale;

	public SpriteRenderer DoorIndicatorSprite;

	//Transforms
	public Transform	m_SpawnPoint;

	void Start () {
		instance = this;
	}
	
	void Update () {

		if(Get_IsGameStart() && !m_bTempCallOnce)
		{
			if(!PlayerScript.instance.Get_PlayerIsAlive())
			{
				Debug.Log("DEADEADAEDAEDAEDAE");
				m_bTempCallOnce = true;
				StartCoroutine(GameEnd());

			}
		}
		if(Input.GetKeyDown(KeyCode.Space))
		{	PlayerScript.instance.Set_PlayerIsAlive(true);
			SetSpawnRound(1);
		}
	}

	public void OpenMaleDoor(bool isDoorOpen)
	{
		if(isDoorOpen)	
		{	m_goDoorMale.GetComponent<DoorScript>().OpenDoor();
			DoorIndicatorSprite.color = new Color (0.353f, 0.679f, 1.000f, 1.000f);
		}
		else
		{	m_goDoorMale.GetComponent<DoorScript>().CloseDoor();
			if(!PlayerScript.instance.Get_PlayerIsAlive())
				return;
			DoorIndicatorSprite.color = new Color (1f, 1f, 1f, 1.000f);
		}
	}

	public void OpenFemaleDoor(bool isDoorOpen)
	{
		if(isDoorOpen)
		{	m_goDoorFemale.GetComponent<DoorScript>().OpenDoor();
			DoorIndicatorSprite.color = new Color (0.994f, 0.596f, 1.000f, 1.000f);
		}
			
		else
		{	m_goDoorFemale.GetComponent<DoorScript>().CloseDoor();
			if(!PlayerScript.instance.Get_PlayerIsAlive())
				return;
			DoorIndicatorSprite.color = new Color (1f, 1f, 1f, 1.000f);
		}
	}

	IEnumerator SpawnPeopleRound(int nSpawnBoy, int nSpawnGirl, float SpawnRate, float DelayforNextSet)
	{
		if(!PlayerScript.instance.Get_PlayerIsAlive())
			yield break;

		while(nSpawnBoy > 0 || nSpawnGirl > 0)
		{
			if(!PlayerScript.instance.Get_PlayerIsAlive())
				yield break;

			if(nSpawnGirl > 0 && Random.Range(0,51) > 25)
			{	SpawnGirl();
				nSpawnGirl--;
			}
			else if (nSpawnBoy > 0)
			{	SpawnBoy();
				nSpawnBoy--;
			}
			yield return new WaitForSeconds(SpawnRate);
		}
		
		if (nSpawnBoy <= 0 && nSpawnGirl <= 0)
		{	if(!PlayerScript.instance.Get_PlayerIsAlive())
				yield break;
			else
			{
				yield return new WaitForSeconds(DelayforNextSet);
				m_nPlayerRound += 1;
				SetSpawnRound(m_nPlayerRound);
			}
		}
	}

	public void SetSpawnRound(int nRound)
	{
		Debug.Log ("ROUND: " + nRound);
		switch(nRound)
		{
		case 0:
			StartCoroutine(SpawnPeopleRound(2, 1, .5f, 2f));
			break;
		case 1:
			StartCoroutine(SpawnPeopleRound(1, 3, .5f, 2f));
			break;
		default:
			StartCoroutine(SpawnPeopleRound(3, 2, .5f, 1f));
			break;
		}
		
	}

	private void SpawnBoy()
	{
		if(!PlayerScript.instance.Get_PlayerIsAlive())
			return;
		GameObject go = Instantiate (m_goBoy, m_SpawnPoint.transform.position, Quaternion.identity) as GameObject;
		go.transform.parent = this.transform;
	}

	private void SpawnGirl()
	{
		if(!PlayerScript.instance.Get_PlayerIsAlive())
			return;
		GameObject go = Instantiate (m_goGirl, m_SpawnPoint.transform.position, Quaternion.identity) as GameObject;
		go.transform.parent = this.transform;
	}

	IEnumerator GameEnd()
	{
		yield return new WaitForSeconds (GameConfig.DELAY_TO_SHOW_RESULT_SCREEN);
		Set_IsGameStart (false);
		OGameMgr.Instance.ShowPopup ("SResultPopup");
	}

	public int Get_LevelRound()
	{	return m_nPlayerRound + 1;
	}

	public bool Get_IsGameStart()
	{	return m_bIsGameStart;
	}

	public void Set_IsGameStart(bool bIsGameStart)
	{	m_bIsGameStart = bIsGameStart;
	}

	public void GameStart(bool isStart)
	{	m_bTempCallOnce = false;
		Set_IsGameStart (true);
		m_nPlayerRound = 0;
		DoorIndicatorSprite.color = Color.white;
		SetSpawnRound(1);
	}
}
