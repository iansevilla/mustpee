﻿using UnityEngine;
using System.Collections;

public class DoorScript : MonoBehaviour {

	public SpriteRenderer DoorSprite;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void OpenDoor()
	{
		this.collider2D.enabled = true;
		DoorSprite.color = new Color(0f,0f,0f,1f);

	}

	public void CloseDoor()
	{
		this.collider2D.enabled = false;
		DoorSprite.color = new Color(0.699f, 0.367f, 0.067f, .5f);
	}
}
