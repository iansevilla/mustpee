
#import <Foundation/Foundation.h>
#import "InMobi.h"
#import "IMConstants.h"
#import "IMBanner.h"
#import "IMBannerDelegate.h"
#import "IMInterstitial.h"
#import "IMInterstitialDelegate.h"
#import "IMIncentivisedDelegate.h"
#import "IMNative.h"
#import "IMNativeDelegate.h"
#import "IMError.h"
#import "InMobiAnalytics.h"
#import "UnityAppController.h"

// Root view controller of Unity screen
extern UIViewController *UnityGetGLViewController();
void UnityPause(bool shouldPause);

@interface InMobiPlugin : NSObject <IMBannerDelegate, IMInterstitialDelegate>
{
}

@property (nonatomic, strong) IMBanner *banner;
@property (nonatomic, strong) IMInterstitial *adInterstitial;

-(id)init;
-(void)initInMobi: (const char*) adUnitID;

-(void) createBannerAd: (const char*) adUnitID;
-(void) createBottomBannerAd: (const char*) adUnitID;
-(void) removeBannerAd;
-(void) bannerDidReceiveAd:(IMBanner *)banner;
-(void) banner:(IMBanner *)banner didFailToReceiveAdWithError:(IMError *)error;
-(void) bannerDidInteract:(IMBanner *)banner withParams:(NSDictionary *)dictionary;
-(void) bannerWillPresentScreen:(IMBanner *)banner;
-(void) bannerWillDismissScreen:(IMBanner *)banner;
-(void) bannerDidDismissScreen:(IMBanner *)banner;
-(void) bannerWillLeaveApplication:(IMBanner *)banner;

-(void) createInterstitialAd: (const char*) adUnitID;
-(bool) isInterstitialAdAvailable;
-(void) showInterstitialAd;
-(void) removeInterstitialAd;
-(void)interstitialDidReceiveAd:(IMInterstitial *)ad;
-(void)interstitial:(IMInterstitial *)ad didFailToReceiveAdWithError:(IMError *)error;
-(void)interstitialWillPresentScreen:(IMInterstitial *)ad;
-(void)interstitialWillDismissScreen:(IMInterstitial *)ad;
-(void)interstitialDidDismissScreen:(IMInterstitial *)ad;
-(void)interstitialWillLeaveApplication:(IMInterstitial *)ad;
-(void)interstitialDidInteract:(IMInterstitial *)ad withParams:(NSDictionary *)dictionary;

@end
