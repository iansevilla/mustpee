#import "GAPlugin.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

@implementation GAPlugin

- (id)init
{
    self = [super init];
    
    [[GAI sharedInstance].logger setLogLevel:kGAILogLevelVerbose];
    [[GAI sharedInstance] setTrackUncaughtExceptions:YES];
    
    return self;
}

-(void) setOptOut: (bool) optOut
{   [[GAI sharedInstance] setOptOut: optOut];
}

-(void) setDryRun: (bool) dryRun
{   [[GAI sharedInstance] setDryRun: dryRun];
}

-(void) setDispatchInterval:(float) interval
{   [[GAI sharedInstance] setDispatchInterval: interval];
}

-(void) dispatch
{   [[GAI sharedInstance] dispatch];
}

-(void) setUserID: (const char*) userID WithID: (const char*) analyticsID
{
    NSLog(@"GAPlugin setUserID %@", [NSString stringWithUTF8String: userID]);
    
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId: [NSString stringWithUTF8String: analyticsID]];
    
    [tracker set:@"&uid" value: [NSString stringWithUTF8String: userID]];
    
}

-(void) sendScreen:(const char*) screenName WithID: (const char*) analyticsID
{
    NSLog(@"GAPlugin sendScreen %@", [NSString stringWithUTF8String: screenName]);
    
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId: [NSString stringWithUTF8String: analyticsID]];
    
    [tracker set:kGAIScreenName value: [NSString stringWithUTF8String: screenName]];
    
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    [tracker set:kGAIScreenName value:nil];
}

-(void) sendEventWithCategory: (const char*) category WithAction: (const char*) action WithLabel: (const char*) label WithValue: (float) value WithID:(const char*) analyticsID
{
    NSLog(@"GAPlugin sendEvent %@", [NSString stringWithUTF8String: action]);
    
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId: [NSString stringWithUTF8String: analyticsID]];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory: [NSString stringWithUTF8String: category]
                                                          action: [NSString stringWithUTF8String: action]
                                                           label: [NSString stringWithUTF8String: label]
                                                           value: [NSNumber numberWithFloat: value]] build]];
}

-(void) sendTimedEventWithCategory: (const char*) category WithInterval: (float) interval WithName: (const char*) name WithLabel: (const char*) label WithID:(const char*) analyticsID
{
    NSLog(@"GAPlugin sendTimedEvent %@", [NSString stringWithUTF8String: name]);
    
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId: [NSString stringWithUTF8String: analyticsID]];
    
    [tracker send:[[GAIDictionaryBuilder createTimingWithCategory: [NSString stringWithUTF8String: category]
                                                         interval: [NSNumber numberWithFloat: interval]
                                                             name: [NSString stringWithUTF8String: name]
                                                            label: [NSString stringWithUTF8String: label]] build]];
}

-(void) sendSocialInteraction: (const char*) socialNetwork WithAction: (const char*) action WithTarget: (const char*) target WithID:(const char*) analyticsID
{
    NSLog(@"GAPlugin sendSocialInteraction %@ %@", [NSString stringWithUTF8String: socialNetwork], [NSString stringWithUTF8String: action]);
    
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId: [NSString stringWithUTF8String: analyticsID]];
    
    [tracker send:[[GAIDictionaryBuilder createSocialWithNetwork: [NSString stringWithUTF8String: socialNetwork]
                                                          action: [NSString stringWithUTF8String: action]
                                                          target: [NSString stringWithUTF8String: target]] build]];
}

-(void) sendPurchaseTransaction: (const char*) transactionID WithName: (const char*) name WithSKU: (const char*) sku WithCategory: (const char*) category WithPrice: (float) price WithQuantity: (int) quantity WithCurrency: (const char*) currency WithID: (const char*) analyticsID
{
    NSLog(@"GAPlugin sendPurchaseTransaction %@", [NSString stringWithUTF8String: name]);
    
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId: [NSString stringWithUTF8String: analyticsID]];
    
    [tracker send:[[GAIDictionaryBuilder createItemWithTransactionId: [NSString stringWithUTF8String: transactionID]
                                                                name: [NSString stringWithUTF8String: name]
                                                                 sku: [NSString stringWithUTF8String: sku]
                                                            category: [NSString stringWithUTF8String: category]
                                                               price: [NSNumber numberWithFloat: price]
                                                            quantity: [NSNumber numberWithInt: quantity]
                                                        currencyCode: [NSString stringWithUTF8String: currency]] build]];
    
}

-(void) sendCrash: (const char*) description IsFatal: (bool) isFatal WithID:(const char*) analyticsID
{
    NSLog(@"GAPlugin sendCrash %@", [NSString stringWithUTF8String: description]);
    
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId: [NSString stringWithUTF8String: analyticsID]];
    
    if(isFatal)
    {   [tracker send:[[GAIDictionaryBuilder
                        createExceptionWithDescription: [NSString stringWithUTF8String: description]
                        withFatal:@YES] build]];
    }   else
    {   [tracker send:[[GAIDictionaryBuilder
                        createExceptionWithDescription: [NSString stringWithUTF8String: description]
                        withFatal:@NO] build]];
    }
}

@end


static GAPlugin *gaPlugin = nil;

extern "C"
{
    void _SetGAOptOut(bool optOut)
    {
        if (gaPlugin == nil)
			gaPlugin = [[GAPlugin alloc] init];
        
        [gaPlugin setOptOut: optOut];
    }
    
    void _SetGADryRun(bool dryRun)
    {
        if (gaPlugin == nil)
			gaPlugin = [[GAPlugin alloc] init];
        
        [gaPlugin setDryRun: dryRun];
    }
    
    void _SetGADispatchInterval(float interval)
    {
        if (gaPlugin == nil)
			gaPlugin = [[GAPlugin alloc] init];
        
        [gaPlugin setDispatchInterval: interval];
    }
    
    void _GADispatch()
    {
        if (gaPlugin == nil)
			gaPlugin = [[GAPlugin alloc] init];
        
        [gaPlugin dispatch];
    }
    
    void _SetGAUserID(const char* userID, const char* analyticsID)
    {
        if (gaPlugin == nil)
			gaPlugin = [[GAPlugin alloc] init];
        
        [gaPlugin setUserID: userID WithID: analyticsID];
    }
    
    void _SendGAScreen(const char* screenName, const char* analyticsID)
    {
        if (gaPlugin == nil)
			gaPlugin = [[GAPlugin alloc] init];
        
        [gaPlugin sendScreen: screenName WithID: analyticsID];
    }
    
    void _SendGAEvent(const char* category, const char* action, const char* label, float value, const char* analyticsID)
    {
        if (gaPlugin == nil)
			gaPlugin = [[GAPlugin alloc] init];
        
        [gaPlugin sendEventWithCategory: category WithAction: action WithLabel: label WithValue: value WithID: analyticsID];
		
    }
    
    void _SendGATimedEvent(const char* category, float interval, const char* name, const char* label, const char* analyticsID)
    {
        if (gaPlugin == nil)
			gaPlugin = [[GAPlugin alloc] init];
        
        [gaPlugin sendTimedEventWithCategory: category WithInterval: interval WithName: name WithLabel: label WithID: analyticsID];
		
    }
    
    void _SendGASocialInteraction(const char* socialNetwork, const char* action, const char* target, const char* analyticsID)
    {
        if (gaPlugin == nil)
			gaPlugin = [[GAPlugin alloc] init];
        
        [gaPlugin sendSocialInteraction: socialNetwork WithAction: action WithTarget: target WithID: analyticsID];
    }
    
    void _SendGAPurchaseTransaction(const char* transactionID, const char* name, const char* sku, const char* category, float price, int quantity, const char* currency, const char* analyticsID)
    {
        if (gaPlugin == nil)
			gaPlugin = [[GAPlugin alloc] init];
        
        [gaPlugin sendPurchaseTransaction: transactionID WithName: name WithSKU: sku WithCategory: category WithPrice: price WithQuantity: quantity WithCurrency: currency WithID: analyticsID];
    }
    
    void _SendGACrash(const char* description, bool isFatal, const char* analyticsID)
    {
        if (gaPlugin == nil)
			gaPlugin = [[GAPlugin alloc] init];
        
        [gaPlugin sendCrash: description IsFatal: isFatal WithID: analyticsID];
        
    }
}