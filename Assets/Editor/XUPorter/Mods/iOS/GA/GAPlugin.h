#import <Foundation/Foundation.h>

@interface GAPlugin : NSObject

-(id)init;
-(void) setOptOut: (bool) optOut;
-(void) setDryRun: (bool) dryRun;
-(void) setDispatchInterval:(float) interval;
-(void) setUserID: (const char*) userID WithID: (const char*) analyticsID;
-(void) sendScreen:(const char*) screenName WithID: (const char*) analyticsID;
-(void) sendEventWithCategory: (const char*) category WithAction: (const char*) action WithLabel: (const char*) label WithValue: (float) value WithID:(const char*) analyticsID;
-(void) sendSocialInteraction: (const char*) socialNetwork WithAction: (const char*) action WithTarget: (const char*) target WithID:(const char*) analyticsID;
-(void) sendCrash: (const char*) description IsFatal: (bool) isFatal WithID:(const char*) analyticsID;

@end
